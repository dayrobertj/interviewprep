//
//  InterviewPrepLibTests.swift
//  InterviewPrepLibTests
//
//  Created by Robert Day on 3/4/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Cocoa
import XCTest
import InterviewPrepLib

class InterviewPrepLibTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStringAreAnagrams() {
        XCTAssertTrue(stringAreAnagrams("Boo", "ooB"))
        XCTAssertFalse(stringAreAnagrams("hello", "Goodbye"))
    }
    
    func testEncodeSpaces() {
        XCTAssertEqual(encodeSpaces("I now have special spaces "), "I%20now%20have%20special%20spaces%20")
    }
    
    
}
