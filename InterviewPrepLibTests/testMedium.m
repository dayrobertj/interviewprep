//
//  testMedium.m
//  InterviewPrep
//
//  Created by Robert Day on 3/12/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>

@interface testMedium : XCTestCase

@end

@implementation testMedium

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (NSInteger)random5 {
    return arc4random_uniform(5) + 1;
}
- (NSInteger)random7 {
    NSInteger total = 0;
    NSInteger limit = 5;
    for(NSInteger i = 1; i <=limit; i++) {
        NSInteger random = [self random5];
        NSLog(@"%ld", (long)random);
        total += random;
    }
    return (total - limit) / 3 + 1;
}
- (void)testBlah {
    
    NSMutableArray *values = [[NSMutableArray alloc] init];
    [values addObject:@(1)];
    [values addObject:@(2)];
    [values addObject:@(3)];
    [values addObject:@(4)];
    [values addObject:@(5)];
    [values addObject:@(6)];
    [values addObject:@(7)];
    for(NSInteger i = 0; i < 100000; i++) {
        NSInteger random = [self random7];
        values[random-1] = @([values[random - 1] integerValue] + 1);
    }
    NSLog(@"Distribution: %@", values);
}

@end
