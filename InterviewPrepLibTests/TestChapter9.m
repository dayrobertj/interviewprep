//
//  TestChapter9.m
//  InterviewPrep
//
//  Created by Robert Day on 3/12/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Chapter9.h"
@interface TestChapter9 : XCTestCase
@property (nonatomic, strong) Chapter9 *myChapter9;
@end

@implementation TestChapter9

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.myChapter9 = [[Chapter9 alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testMergeTwoArrays {
    NSArray *arrayToMerge = @[@4, @6, @9, @10];
    NSMutableArray *holdingArray = [NSMutableArray array];
    [holdingArray addObject:@1];
    [holdingArray addObject:@3];
    [holdingArray addObject:@5];
    [holdingArray addObject:@7];
    [holdingArray addObject:@8];
    [holdingArray addObject:@11];
    [holdingArray addObject:@15];
    [holdingArray addObject:[NSNull null]];
    [holdingArray addObject:[NSNull null]];
    [holdingArray addObject:[NSNull null]];
    [holdingArray addObject:[NSNull null]];

    
    [self.myChapter9 mergeTwoArraysWithHoldingArray:holdingArray toMergeArray:arrayToMerge];
    NSArray *result =  @[@1, @3, @4, @5, @6,@7,@8,@9,@10,@11,@15];
    XCTAssertEqualObjects(holdingArray, result);
    
    NSLog(@"%@", self.myChapter9);

}

-(void)testFindNumberInRotatedSortedArray {
//    5 6 7 8 9 10 11 1 2 3 4
    NSArray *searchArray = @[@5,@6,@7,@8,@9,@10,@11,@1,@2,@3,@4];
//    XCTAssertEqual([self.myChapter9 findNumber:16 inSortedAndShiftedArray:searchArray], -1);
    XCTAssertEqual([self.myChapter9 findNumber:1 inSortedAndShiftedArray:searchArray], 7);
    XCTAssertEqual([self.myChapter9 findNumber:2 inSortedAndShiftedArray:searchArray], 8);
    XCTAssertEqual([self.myChapter9 findNumber:3 inSortedAndShiftedArray:searchArray], 9);
    XCTAssertEqual([self.myChapter9 findNumber:4 inSortedAndShiftedArray:searchArray], 10);
    XCTAssertEqual([self.myChapter9 findNumber:5 inSortedAndShiftedArray:searchArray], 0);
    XCTAssertEqual([self.myChapter9 findNumber:6 inSortedAndShiftedArray:searchArray], 1);
    XCTAssertEqual([self.myChapter9 findNumber:7 inSortedAndShiftedArray:searchArray], 2);
    XCTAssertEqual([self.myChapter9 findNumber:8 inSortedAndShiftedArray:searchArray], 3);
    XCTAssertEqual([self.myChapter9 findNumber:9 inSortedAndShiftedArray:searchArray], 4);
    XCTAssertEqual([self.myChapter9 findNumber:10 inSortedAndShiftedArray:searchArray],5);
    XCTAssertEqual([self.myChapter9 findNumber:11 inSortedAndShiftedArray:searchArray], 6);
}

-(void)testFindStringInArrayWithQuotes {
    NSArray *input = @[@"at", @"", @"", @"", @"ball", @"", @"", @"car", @"", @"", @"dad", @"", @""];
    
    XCTAssertEqual([self.myChapter9 findIndexOfString:@"at" inArray:input], 0);
    XCTAssertEqual([self.myChapter9 findIndexOfString:@"ball" inArray:input], 4);
    XCTAssertEqual([self.myChapter9 findIndexOfString:@"car" inArray:input], 7);
    XCTAssertEqual([self.myChapter9 findIndexOfString:@"dad" inArray:input], 10);
    XCTAssertEqual([self.myChapter9 findIndexOfString:@"ballcar" inArray:input], -1);
}

-(void)testFindElementInSortedMatrix {
//- (CGPoint)findElement:(NSInteger)element inMatrix:(NSArray *)matrix {
    NSArray *firstRow = @[@0, @10, @20, @30, @40];
    NSArray *secondRow = @[@5, @11, @21, @31, @41];
    NSArray *thirdRow = @[@10, @12, @22, @32, @42];
    NSArray *forthRow = @[@15, @16, @23, @33, @43];
    NSArray *fifthRow = @[@20, @21, @24, @34, @44];
    NSArray *input = @[firstRow, secondRow, thirdRow, forthRow, fifthRow];
    XCTAssertTrue(CGPointEqualToPoint([self.myChapter9 findElement:100 inMatrix:input], CGPointZero));
    
    XCTAssertTrue(CGPointEqualToPoint([self.myChapter9 findElement:16 inMatrix:input], CGPointMake(3,1)));
    
    XCTAssertTrue(CGPointEqualToPoint([self.myChapter9 findElement:22 inMatrix:input], CGPointMake(2, 2)));
    
    XCTAssertTrue(CGPointEqualToPoint([self.myChapter9 findElement:24 inMatrix:input], CGPointMake(4, 2)));
    
    XCTAssertTrue(CGPointEqualToPoint([self.myChapter9 findElement:15 inMatrix:input], CGPointMake(3, 0)));
    
    

    
}

-(void)testBestCircusTower {
    Person *performer1 = [[Person alloc] initWithHeight:56 weight:150];
    Person *performer2 = [[Person alloc] initWithHeight:60 weight:160];
    Person *performer3 = [[Person alloc] initWithHeight:68 weight:120];
    Person *performer4 = [[Person alloc] initWithHeight:70 weight:140];
    Person *performer5 = [[Person alloc] initWithHeight:75 weight:145];
    Person *performer6 = [[Person alloc] initWithHeight:78 weight:100];
//    Person *performer1 = [[Person alloc] initWithHeight:60 weight:100];
//    Person *performer2 = [[Person alloc] initWithHeight:70 weight:150];
//    Person *performer3 = [[Person alloc] initWithHeight:56 weight:90];
//    Person *performer4 = [[Person alloc] initWithHeight:75 weight:190];
//    Person *performer5 = [[Person alloc] initWithHeight:60 weight:96];
//    Person *performer6 = [[Person alloc] initWithHeight:68 weight:110];
    NSArray *performers = @[performer1, performer2, performer3, performer4, performer5, performer6];
    
    NSArray *maxList = [self.myChapter9 bestCircusTowerWithPerformers:performers];
    Person *blah = maxList[0];
    for(Person *person in maxList) {
        NSLog(@"%@", person);
    }
    NSLog(@"HERE");
}

@end
