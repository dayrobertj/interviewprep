//
//  TestChapter1.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/15/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Cocoa
import XCTest
import InterviewPrepLib

class TestChapter1: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testRotateArray() {
        let row1 = ["A","B","C","D"]
        let row2 = ["E","F","G","H"]
        let row3 = ["I","J","K","L"]
        let row4 = ["M","N","O","P"]
        var fullarray = [row1, row2, row3, row4]
        rotateArray90Degrees(&fullarray)
        println("\(fullarray)")
        
    }


}
