//
//  TestRandomSwift.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/17/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Cocoa
import XCTest
import InterviewPrepLib

class TestRandomSwift: XCTestCase {
    var randomSwiftProblems = RandomSwiftProblems()
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

   
    func testPrintReverseLinkedList() {
        var list = LinkedList()
        list.appendNode(ListNode(value: 5))
        list.appendNode(ListNode(value: 4))
        list.appendNode(ListNode(value: 3))
        list.appendNode(ListNode(value: 2))
        list.appendNode(ListNode(value: 1))
        
        randomSwiftProblems.printLinkedListReversedRecursively(list, currentNode: list.firstNode)
    }
    
    func testQueueFromStacks() {
        var q = QueueFromStacks<Int>()
        q.enqueue(1)
        q.enqueue(2)
        q.enqueue(3)
        q.enqueue(4)
        q.enqueue(5)
        XCTAssertEqual(q.dequeue()!, 1);
        XCTAssertEqual(q.dequeue()!, 2);
        XCTAssertEqual(q.dequeue()!, 3);
        XCTAssertEqual(q.dequeue()!, 4);
        XCTAssertEqual(q.dequeue()!, 5);

    }
    
    func testHashTable() {
        
//        var hashTable = HashTable<Int, String>()
//        hashTable.addObject("Five", forKey: 5)
//        hashTable.addObject("Six", forKey: 6)
//        hashTable.addObject("One Oh Five", forKey: 105)
//        
//        XCTAssertEqual(hashTable.objectForKey(5)!, "Five")
//        XCTAssertEqual(hashTable.objectForKey(6)!, "Six")
//        XCTAssertEqual(hashTable.objectForKey(105)!, "One Oh Five")
//        XCTAssertNil(hashTable.objectForKey(1))
        var hashTable = HashTable<String, Int>()
        hashTable.addObject(5, forKey: "Five")
        hashTable.addObject(6, forKey: "Six")
        hashTable.addObject(105, forKey: "One Oh Five")
        hashTable.addObject(11, forKey: "Eleven")
        hashTable.addObject(15, forKey: "Fifteen")
        
        XCTAssertEqual(hashTable.objectForKey("Five")!, 5)
        XCTAssertEqual(hashTable.objectForKey("Six")!, 6)
        XCTAssertEqual(hashTable.objectForKey("One Oh Five")!, 105)
        XCTAssertNil(hashTable.objectForKey("One"))
        
        hashTable.removeObjectWithKey("Five")
        XCTAssertNil(hashTable.objectForKey("Five"))
        
        
    }
    
    
    func testHeap() {
        var heap = MinHeap<Int>()
        var input = [5,3,2,7,8,9,10,100,1000,50,75]
        heap.buildMinHeap(input)
        
        XCTAssertEqual(heap.extractMin()!, 2)
        XCTAssertEqual(heap.extractMin()!, 3)
        XCTAssertEqual(heap.extractMin()!, 5)
        XCTAssertEqual(heap.extractMin()!, 7)
        XCTAssertEqual(heap.extractMin()!, 8)
        XCTAssertEqual(heap.extractMin()!, 9)
        XCTAssertEqual(heap.extractMin()!, 10)
        XCTAssertEqual(heap.extractMin()!, 50)
        XCTAssertEqual(heap.extractMin()!, 75)
        XCTAssertEqual(heap.extractMin()!, 100)
        XCTAssertEqual(heap.extractMin()!, 1000)
        
        XCTAssertNil(heap.extractMin())
    }

}
