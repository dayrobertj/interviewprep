//
//  TestChapter4.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/7/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Cocoa
import XCTest
import InterviewPrepLib

class TestChapter4: XCTestCase {
    let myTree = BinarySearchTree<Int>()
    var balancedTree : BinarySearchTree<Int>!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        myTree.insertNode(TreeNode<Int>(value: 10))
        myTree.insertNode(TreeNode<Int>(value: 1))
        myTree.insertNode(TreeNode<Int>(value: 4))
        myTree.insertNode(TreeNode<Int>(value: 5))
        myTree.insertNode(TreeNode<Int>(value: 7))
        myTree.insertNode(TreeNode<Int>(value: 6))
        myTree.insertNode(TreeNode<Int>(value: 11))
        myTree.insertNode(TreeNode<Int>(value: 13))
        myTree.insertNode(TreeNode<Int>(value: 16))
        myTree.insertNode(TreeNode<Int>(value: 12))
        myTree.insertNode(TreeNode<Int>(value: 15))
        myTree.insertNode(TreeNode<Int>(value: 20))
        myTree.insertNode(TreeNode<Int>(value: 25))
        myTree.insertNode(TreeNode<Int>(value: 21))
        myTree.insertNode(TreeNode<Int>(value: 22))
        myTree.insertNode(TreeNode<Int>(value: 23))
        myTree.insertNode(TreeNode<Int>(value: 40))
        myTree.insertNode(TreeNode<Int>(value: 30))
        myTree.insertNode(TreeNode<Int>(value: 27))
        
        var values = [1,4,5,6,7,10,11,12,13,15,16,20,21,22,23,25,27,30,40]
        balancedTree = balancedTreeFromSortedArray(values)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testIsTreeBalanced() {
        
        XCTAssertFalse(isTreeBalanced(myTree))
    }
    
    func testBalancedTreeFromSortedArray() {
        var values = [1,4,5,6,7,10,11,12,13,15,16,20,21,22,23,25,27,30,40]
        let newTree = balancedTreeFromSortedArray(values)
        
        println(newTree.bfsDescription())
        
        XCTAssertTrue(isTreeBalanced(newTree))
        println("HERE")
    }

    
    func testLinkedListsFromTreeLevels() {
        
        var lists = linkedListsFromTreeLevels(balancedTree)
        println("HERE")
    }
    
    func testCommonAncestorOfNodes() {
        var myTree = balancedTree
        var node1 = TreeNode<Int>(value: 7)
        var node2 = TreeNode<Int>(value: 23)
        
        var ancestor = commonAncestorOfNodes(myTree, node1, node2)
        XCTAssertEqual(ancestor!.value, 15)
        
        var ancestor2 = commonAncestorOfNodes(myTree, TreeNode<Int>(value: 1), TreeNode<Int>(value: 10))
        XCTAssertEqual(ancestor2!.value, 7)
        
        var ancestor3 = commonAncestorOfNodes(myTree, TreeNode<Int>(value: 30), TreeNode<Int>(value: 21))
        XCTAssertEqual(ancestor3!.value, 23)
        
        var ancestor4 = commonAncestorOfNodes(myTree, TreeNode<Int>(value: 30), TreeNode<Int>(value: 65))
        XCTAssertNil(ancestor4)
        
        var ancestor5 = commonAncestorOfNodes(myTree, TreeNode<Int>(value: 23), TreeNode<Int>(value: 16))
        XCTAssertEqual(ancestor5!.value, 23)
    }
    
    func testIsTreeSubtree() {
        var tree1 : BinaryTree = self.balancedTree
        var tree2 = BinaryTree<Int>()
        tree2.insertNode(TreeNode<Int>(value:21))
        tree2.insertNode(TreeNode<Int>(value:20))
        tree2.insertNode(TreeNode<Int>(value:22))
        tree2.insertNode(TreeNode<Int>(value:16))
        
        XCTAssertTrue(isTreeSubtree(tree1, tree2))
        
        tree2.insertNode(TreeNode<Int>(value:12))
        XCTAssertFalse(isTreeSubtree(tree1, tree2))
    }

    
    
    //TODO: In lectures, they do this with an array of vertices and an array of edges
    // What do we think is better? How would we associate weights with these?
    func testRouteBetweenNodesInGraph() {
        var graph = Array<[Int]>(count:7, repeatedValue: [Int]())
        graph[0] = [1,2]
        graph[1] = []
        graph[2] = [1,3]
        graph[3] = [4]
        graph[4] = [3,5]
        graph[5] = [4]
        graph[6] = []
        
        XCTAssertTrue(routeBetweenNodesInGraph(graph, 0, 5))
        XCTAssertFalse(routeBetweenNodesInGraph(graph, 0, 6))
    }
}


