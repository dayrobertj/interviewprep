//
//  DataStructureTests.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/6/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Cocoa
import XCTest
import InterviewPrepLib

class DataStructureTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testBinarySearchTree() {
        let myTree = BinarySearchTree<Int>()
        myTree.insertNode(TreeNode<Int>(value: 10))
        myTree.insertNode(TreeNode<Int>(value: 1))
        myTree.insertNode(TreeNode<Int>(value: 4))
        myTree.insertNode(TreeNode<Int>(value: 5))
        myTree.insertNode(TreeNode<Int>(value: 7))
        myTree.insertNode(TreeNode<Int>(value: 6))
        myTree.insertNode(TreeNode<Int>(value: 11))
        myTree.insertNode(TreeNode<Int>(value: 13))
        myTree.insertNode(TreeNode<Int>(value: 16))
        myTree.insertNode(TreeNode<Int>(value: 12))
        myTree.insertNode(TreeNode<Int>(value: 15))
        myTree.insertNode(TreeNode<Int>(value: 20))
        myTree.insertNode(TreeNode<Int>(value: 25))
        myTree.insertNode(TreeNode<Int>(value: 21))
        myTree.insertNode(TreeNode<Int>(value: 22))
        myTree.insertNode(TreeNode<Int>(value: 23))
        myTree.insertNode(TreeNode<Int>(value: 40))
        myTree.insertNode(TreeNode<Int>(value: 30))
        myTree.insertNode(TreeNode<Int>(value: 27))
        
        println(myTree.bfsDescription())
        
        println(myTree.descriptionInOrder())
        
        var searchedNode = myTree.searchNodeWithValue(16)
        
        myTree.deleteNode(searchedNode!)
        println(myTree.descriptionInOrder())
        var height = myTree.heightOfTree()
    }
    
    
    func testQuickSort() {
        var sortable = [2,5,6,8,9,1,3,5,6,7,100,25]
        var quickSort = QuickSort()
        quickSort.QuickSort(&sortable)
        println("Sorted: \(sortable)")
    }

}
