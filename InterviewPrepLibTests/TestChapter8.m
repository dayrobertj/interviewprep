//
//  TestChapter8.m
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Chapter8.h"

@interface TestChapter8 : XCTestCase

@end

@implementation TestChapter8

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testfibonacciNumberAtIndex {
    XCTAssertEqual([Chapter8 fibonacciNumberAtIndex:5], 8);
    XCTAssertEqual([Chapter8 fibonacciNumberAtIndex:30], 1346269);
}

- (void)testAllSubsetsOfSet {
    NSArray *set = @[@1,@2,@3,@4, @5];
    NSArray *allSubsets = [Chapter8 allSubsetsOfSet:set];
    NSLog(@"%@", allSubsets);
}

- (void)testAllPermutationsOfString {
    Chapter8 *myChapter8 = [[Chapter8 alloc] init];
    NSDate *firstDate = [NSDate date];
    NSLog(@"FirstStart: %@", [NSDate date]);
    NSArray *allPermutations = [myChapter8 permutateString:@"ABCDEFGHIJK"];
    NSTimeInterval firstInteveral = [[NSDate date] timeIntervalSinceDate:firstDate];
    NSLog(@"FirstEnd: %f", firstInteveral);
    NSLog(@"First length %lu", (unsigned long)allPermutations.count);


    NSDate *secondDate = [NSDate date];
    NSLog(@"SecondStart: %@", [NSDate date]);
    NSArray *perms = [myChapter8 blah:@"ABCDEFGHIJK"];
    NSTimeInterval secondInterval = [[NSDate date] timeIntervalSinceDate:secondDate];
    NSLog(@"SecondEnd: %f", secondInterval);
    NSLog(@"Second length %lu", (unsigned long)perms.count);
    
    NSDate *thirdDate = [NSDate date];
    NSArray *thirdResults = [myChapter8 blah2:@"ABCDEFGHIJK" index:10];
    NSTimeInterval thirdInterval = [[NSDate date] timeIntervalSinceDate:thirdDate];
    NSLog(@"Third End: %f", thirdInterval);
    NSLog(@"Third length %lu", (unsigned long)thirdResults.count);
}

- (void)testAllPermutationsForParens {
    Chapter8 *myChapter8 = [Chapter8 new];
    NSArray *parens = @[@"()", @"()", @"()", @"()"];
    NSArray *permutations = [myChapter8 allPermutationsForParens:parens atIndex:parens.count - 1];
    NSLog(@"HERE");
    
}


- (void)testPaintFill {
    Chapter8 *myChapter8 = [Chapter8 new];
    NSMutableArray *grid = [NSMutableArray new];
    [grid addObject:[NSMutableArray arrayWithObjects:@0,@0,@0,@0,@0, nil]];
    [grid addObject:[NSMutableArray arrayWithObjects:@1,@1,@1,@1,@1, nil]];
    [grid addObject:[NSMutableArray arrayWithObjects:@0,@0,@0,@0,@0, nil]];
    [grid addObject:[NSMutableArray arrayWithObjects:@0,@0,@0,@0,@0, nil]];
    [grid addObject:[NSMutableArray arrayWithObjects:@0,@0,@0,@0,@0, nil]];
    
    NSArray *startingPoint = @[@2, @2];
    NSArray *result = [myChapter8 paintFillWithArray:grid startingPoint:startingPoint color:@1];
}

- (void)testMakeChange {
    Chapter8 *myChapter8 = [Chapter8 new];
    NSArray *change = [myChapter8 makeChangeForCents:100 coinIndex:3];
    NSLog(@"GOT HERE");
}


@end
