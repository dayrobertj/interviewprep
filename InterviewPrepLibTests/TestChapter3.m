//
//  TestChapter3.m
//  InterviewPrep
//
//  Created by Robert Day on 3/9/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Chapter3.h"

@interface TestChapter3 : XCTestCase

@end

@implementation TestChapter3

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSetAllBitsInN {
    Chapter3 *myChapter3 = [[Chapter3 alloc] init];
    NSNumber *n = @(1024);
    NSNumber *m = @(21);
    NSNumber *result = [myChapter3 setAllBitsInN:n toM:m betweenIndexI:2 andJ:6];
}

- (void)testSmallerAndLargerNumber {
    Chapter3 *myChapter3 = [[Chapter3 alloc] init];
    [myChapter3 printNextBinarySmallestAndLargestOfNumber:@(15)];
}

-(void)testSwapEvenAndOddDigits {
//    - (NSNumber *)swapEvenAndOnBitsInNumber:(NSNumber *)number {
    Chapter3 *myChapter3 = [[Chapter3 alloc] init];
    NSNumber *result = [myChapter3 swapEvenAndOnBitsInNumber:@(204)];
}

@end
