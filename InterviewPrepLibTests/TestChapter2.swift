//
//  TestChapter2.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/4/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Cocoa
import XCTest
import InterviewPrepLib

class TestChapter2: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testRemoveDuplicatesFromLinkedList() {
        var list = LinkedList()
        list.appendNode(ListNode(value:10))
        list.appendNode(ListNode(value:20))
        list.appendNode(ListNode(value:30))
        list.appendNode(ListNode(value:40))
        list.appendNode(ListNode(value:50))
        list.appendNode(ListNode(value:60))
        list.appendNode(ListNode(value:70))
        list.appendNode(ListNode(value:70))
        list.appendNode(ListNode(value:20))
        list.appendNode(ListNode(value:90))
        
        
        var priorNodeCount = 0
        var firstNode = list.firstNode
        while firstNode != nil {
            priorNodeCount += 1
            firstNode = firstNode!.next
        }
        
        removeDuplicatesFromLinkedList(list)
        
        var afterNodeCount = 0
        println("____")
        
        firstNode = list.firstNode
        while firstNode != nil {
            println(firstNode!.value)
            afterNodeCount += 1
            firstNode = firstNode!.next
        }
        
        XCTAssertEqual(priorNodeCount - 2, afterNodeCount)
        
        

    }
    
    func testFindNthLastElementInLinkedList() {
        var list = LinkedList()
        list.appendNode(ListNode(value:10))
        list.appendNode(ListNode(value:20))
        list.appendNode(ListNode(value:30))
        list.appendNode(ListNode(value:40))
        list.appendNode(ListNode(value:50))
        list.appendNode(ListNode(value:60))
        list.appendNode(ListNode(value:70))
        list.appendNode(ListNode(value:70))
        list.appendNode(ListNode(value:20))
        list.appendNode(ListNode(value:90))
        

        
        var fifthLastElement = findNthLastElementInLinkedList(list, 5)
        var intValue = fifthLastElement!.value.integerValue
        XCTAssertEqual(intValue, 60)
    }
    
    func testDeleteNode() {
        var list = LinkedList()
        var thirdNode = ListNode(value:30)
        list.appendNode(ListNode(value:10))
        list.appendNode(ListNode(value:20))
        list.appendNode(thirdNode)
        list.appendNode(ListNode(value:40))
        list.appendNode(ListNode(value:50))
        list.appendNode(ListNode(value:60))
        list.appendNode(ListNode(value:70))
        list.appendNode(ListNode(value:70))
        list.appendNode(ListNode(value:20))
        list.appendNode(ListNode(value:90))
        
        deleteNodeFromList(thirdNode)
        
        var nodeCount = 0
        var firstNode = list.firstNode
        while firstNode != nil {
            nodeCount += 1
            firstNode = firstNode!.next
        }
        
        XCTAssertEqual(nodeCount, 9)
    }
    
    func testSumifyLinkedList() {
        var list1 = LinkedList()
        var list2 = LinkedList()
        
        list1.appendNode(ListNode(value:3))
        list1.appendNode(ListNode(value:1))
        list1.appendNode(ListNode(value:5))
        
        list2.appendNode(ListNode(value:5))
        list2.appendNode(ListNode(value:9))
        list2.appendNode(ListNode(value:2))
        
        var newList = sumifyLinkedList(list1, list2)
        var firstNode = newList.firstNode
        XCTAssertEqual(newList.firstNode!.value.integerValue, 8)
        XCTAssertEqual(newList.firstNode!.next.value.integerValue, 0)
        XCTAssertEqual(newList.firstNode!.next.next.value.integerValue, 8)
//        while firstNode != nil {
//            println(firstNode!.value)
//            firstNode = firstNode!.next
//        }
    }
    func testSortStackAscending() {
        var stack = Stack<Int>()
        stack.push(5)
        stack.push(10)
        stack.push(3)
        stack.push(7)
        stack.push(11)
        
        sortStackAscending(stack)
        
        var firstNode = stack.pop()
        while firstNode != nil {
            println(firstNode!)
            firstNode = stack.pop()
        }
    }
}


