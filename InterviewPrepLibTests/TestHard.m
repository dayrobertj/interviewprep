//
//  TestHard.m
//  InterviewPrep
//
//  Created by Robert Day on 3/14/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Hard.h"

@interface TestHard : XCTestCase
@property (nonatomic, strong) Hard *hard;
@end

@implementation TestHard

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _hard = [Hard new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testShuffleDeck {
    NSArray *shuffled = [self.hard shuffledDeck];
    NSLog(@"%@", shuffled);
}


- (void)testBookWordDifference {
//    - (NSInteger)shortestDistanceBetweenWordsInBook:(NSArray *)book withFirstWord:(NSString *)firstWord secondWord:(NSString *)secondWord {
    NSArray *book = @[@"Once", @"upon", @"a", @"time", @"a", @"small", @"dog", @"ran", @"away", @"he", @"ran", @"very", @"far", @"away"];
    
    XCTAssertEqual([self.hard shortestDistanceBetweenWordsInBook:book withFirstWord:@"away" secondWord:@"ran"], 1);
    XCTAssertEqual([self.hard shortestDistanceBetweenWordsInBook:book withFirstWord:@"a" secondWord:@"small"], 1);
    XCTAssertEqual([self.hard shortestDistanceBetweenWordsInBook:book withFirstWord:@"away" secondWord:@"very"], 2);
    XCTAssertEqual([self.hard shortestDistanceBetweenWordsInBook:book withFirstWord:@"small" secondWord:@"cat"], NSIntegerMax);
}

//- (NSString *)longestWordFromOtherWordsInArray:(NSArray *)words {
-(void)testLongestWordFromOtherWordsInArray {
    NSArray *words = @[@"test", @"tester", @"testertest", @"testing", @"testingtester"];
    XCTAssertEqual([self.hard longestWordFromOtherWordsInArray:words], @"testingtester");

}

- (void)testLargestBlackSubarray {
//    - (NSArray *)largestBlackSubsquareInArray:(NSArray *)array {
    NSArray *row1 = @[@"W", @"B", @"B", @"W", @"W"];
    NSArray *row2 = @[@"W", @"B", @"B", @"B", @"W"];
    NSArray *row3 = @[@"W", @"B", @"B", @"W", @"W"];
    NSArray *row4 = @[@"W", @"B", @"B", @"B", @"W"];
    NSArray *row5 = @[@"B", @"B", @"B", @"W", @"B"];
    
    NSArray *array = @[row1, row2, row3, row4, row5];
    [self.hard largestBlackSubsquareInArray:array];
    
    NSArray *row1b = @[@"B", @"B", @"B", @"W", @"W"];
    NSArray *row2b = @[@"W", @"B", @"B", @"W", @"W"];
    NSArray *row3b = @[@"W", @"B", @"B", @"B", @"W"];
    NSArray *row4b = @[@"W", @"B", @"W", @"B", @"W"];
    NSArray *row5b = @[@"B", @"B", @"B", @"W", @"B"];
    
    NSArray *array1 = @[row1b, row2b, row3b, row4b, row5b];
    [self.hard largestBlackSubsquareInArray:array1];
    
    NSArray *row1bc = @[@"B", @"B", @"B", @"B", @"B"];
    NSArray *row2bc = @[@"B", @"B", @"B", @"B", @"B"];
    NSArray *row3bc = @[@"B", @"B", @"B", @"B", @"B"];
    NSArray *row4bc = @[@"B", @"B", @"B", @"B", @"B"];
    NSArray *row5bc = @[@"B", @"B", @"B", @"B", @"B"];
    
    NSArray *array2 = @[row1bc, row2bc, row3bc, row4bc, row5bc];
        [self.hard largestBlackSubsquareInArray:array2];
    
    NSArray *row1a = @[@"W", @"B", @"B", @"W", @"W"];
    NSArray *row2a = @[@"W", @"B", @"B", @"B", @"W"];
    NSArray *row3a = @[@"W", @"B", @"B", @"B", @"W"];
    NSArray *row4a = @[@"W", @"B", @"B", @"B", @"W"];
    NSArray *row5a = @[@"B", @"B", @"B", @"W", @"B"];
    
    NSArray *array3 = @[row1a, row2a, row3a, row4a, row5a];
    [self.hard largestBlackSubsquareInArray:array3];
}

@end
