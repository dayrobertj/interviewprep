//
//  testRandom.m
//  InterviewPrep
//
//  Created by Robert Day on 3/17/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "RandomProblems.h"

@interface testRandom : XCTestCase
@property(nonatomic, strong) RandomProblems *randomProblems;
@end

@implementation testRandom

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _randomProblems = [RandomProblems new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void)testWordsForPhoneNumber {
    NSArray *phoneNumber = @[@9,@0,@8,@5,@2,@8,@0,@7,@4,@2];
    NSArray *permutations = [self.randomProblems wordPermutationsForNumber:phoneNumber];
    NSLog(@"Total permutations: %ld", (long)permutations.count);
}

-(void)testMove0sRight {
//    - (void)move0sInArrayToRight:(NSMutableArray *)array {
    NSMutableArray *array = [NSMutableArray arrayWithArray:@[@3,@4,@8,@0,@0,@1,@11,@25,@0, @0, @12,@0]];
    [self.randomProblems move0sInArrayToRight:array];
    NSLog(@"%@", array);
}

-(void)testSumUpLastK{
    
//    -(NSInteger)sumUpLastK:(NSInteger)k integersOfArray:(NSArray *)array {
    NSArray *array = @[@3,@5,@7,@1,@9,@3,@1,@8,@7];
    XCTAssertEqual([self.randomProblems sumUpLastK:1 integersOfArray:array], 9);
    XCTAssertEqual([self.randomProblems sumUpLastK:2 integersOfArray:array], 17);
    XCTAssertEqual([self.randomProblems sumUpLastK:3 integersOfArray:array], 24);
    XCTAssertEqual([self.randomProblems sumUpLastK:4 integersOfArray:array], 31);
    XCTAssertEqual([self.randomProblems sumUpLastK:5 integersOfArray:array], 36);
    XCTAssertEqual([self.randomProblems sumUpLastK:6 integersOfArray:array], 39);
    XCTAssertEqual([self.randomProblems sumUpLastK:7 integersOfArray:array], 42);
    XCTAssertEqual([self.randomProblems sumUpLastK:8 integersOfArray:array], 43);
    XCTAssertEqual([self.randomProblems sumUpLastK:9 integersOfArray:array], 44);
}

@end
