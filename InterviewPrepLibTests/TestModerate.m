//
//  TestModerate.m
//  InterviewPrep
//
//  Created by Robert Day on 3/14/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "Moderate.h"
@interface TestModerate : XCTestCase
@property (nonatomic, strong) Moderate *moderate;
@end

@implementation TestModerate

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _moderate = [[Moderate alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testSomeoneWonTicTacToe {
    NSArray *row0 = @[@1, @2, @1];
    NSArray *row1 = @[@2, @2, @1];
    NSArray *row2 = @[@1, @2, @-1];
    
    NSArray *board = @[row0, row1, row2];
    BOOL result = [self.moderate someoneWonTicTacToeWithBoard:board];
}

@end
