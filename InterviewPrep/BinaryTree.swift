//
//  BinaryTree.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation


public class BinaryTree<T:Comparable> : Tree<T> {

    
    override public init() {}
    public func searchNodeWithValue(value: T) -> TreeNode<T>? {
        if let root = root {
            var q = Queue<TreeNode<T>>()
            q.enqueue(root)
            while !q.isEmpty() {
                var node = q.dequeue()!
                var nodeVal = node.value
                if nodeVal == value {
                    return node
                } else {
                    if let left = node.leftChild {
                        q.enqueue(left)
                    }
                    if let right = node.rightChild {
                        q.enqueue(right)
                    }
                }
                
            }
            return nil
        } else {
            return nil
        }
        
    }
    
    //While this is how we insert into a binary search tree, it works fine for this
    public func insertNode(node: TreeNode<T>) {
        insertNode(root, parentNode: nil, newNode: node)
    }
    
    private func insertNode(rootNode: TreeNode<T>?, parentNode: TreeNode<T>?, newNode: TreeNode<T>) {
        if let rootNode = rootNode {
            if newNode.value < rootNode.value {
                if rootNode.leftChild != nil {
                    insertNode(rootNode.leftChild, parentNode: rootNode, newNode: newNode)
                } else {
                    rootNode.leftChild = newNode
                    rootNode.parent = parentNode
                }
            } else {
                if rootNode.rightChild != nil {
                    insertNode(rootNode.rightChild, parentNode: rootNode, newNode: newNode)
                } else {
                    rootNode.rightChild = newNode
                    rootNode.parent = parentNode
                }
            }
        } else {
            root = newNode
        }
        
    }
    
    
}