//
//  BinarySearchTree.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/5/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class BinarySearchTree<T:Comparable> : BinaryTree<T> {
    override public init() {}
    public override func searchNodeWithValue(value: T) -> TreeNode<T>? {
        println("Root is \(root?.value)")
        return searchNodeFromRoot(root, value: value)
    }
    
    private func searchNodeFromRoot(rootNode: TreeNode<T>?, value: T) -> TreeNode<T>? {
        if let rootNode = rootNode {
            if rootNode.value == value {
                return rootNode
            } else if value < rootNode.value {
                return searchNodeFromRoot(rootNode.leftChild, value: value)
            } else {
                return searchNodeFromRoot(rootNode.rightChild, value: value)
            }
        } else {
            return nil
        }
    }
    
//    public func insertNode(node: TreeNode<T>) {
//        insertNode(root, parentNode: nil, newNode: node)
//    }
//
//    private func insertNode(rootNode: TreeNode<T>?, parentNode: TreeNode<T>?, newNode: TreeNode<T>) {
//        if let rootNode = rootNode {
//            if newNode.value < rootNode.value {
//                if rootNode.leftChild != nil {
//                    insertNode(rootNode.leftChild, parentNode: rootNode, newNode: newNode)
//                } else {
//                    rootNode.leftChild = newNode
//                    rootNode.parent = parentNode
//                }
//            } else {
//                if rootNode.rightChild != nil {
//                    insertNode(rootNode.rightChild, parentNode: rootNode, newNode: newNode)
//                } else {
//                    rootNode.rightChild = newNode
//                    rootNode.parent = parentNode
//                }
//            }
//        } else {
//            root = newNode
//        }
//        
//    }
    // This node must be in the tree already,we could wrap the first public call to this with a call to
    // search for the node but all future calls can't do that
    public func deleteNode(node: TreeNode<T>) -> TreeNode<T>? {
        var locatedNode = node
        if let parentNode = locatedNode.parent {
            if locatedNode.leftChild == nil  && locatedNode.rightChild == nil {
                if parentNode.rightChild != nil && parentNode.rightChild!.value == locatedNode.value {
                    parentNode.rightChild = nil
                } else {
                    parentNode.leftChild = nil
                }
            } else if locatedNode.leftChild != nil  && locatedNode.rightChild != nil {
                //We have two children
                //Let's swap 'node' with it's successor & then delete the successor
                var nodeToSwap = successorOfNode(locatedNode)! // Successor can't be nil if we've gotten to this place as our node has two children
                locatedNode.value = nodeToSwap.value
                deleteNode(nodeToSwap)
            } else if locatedNode.leftChild != nil {
                // We have a left child
                if parentNode.rightChild != nil && parentNode.rightChild!.value == locatedNode.value {
                    parentNode.rightChild = locatedNode.leftChild
                } else {
                    parentNode.leftChild = locatedNode.leftChild
                }
            } else if locatedNode.rightChild != nil {
                //We have a right child
                if parentNode.rightChild != nil && parentNode.rightChild!.value == locatedNode.value {
                    parentNode.rightChild = locatedNode.rightChild
                } else {
                    parentNode.leftChild = locatedNode.rightChild
                }
            }
        } else {
            //We must be the root
            self.root = nil
        }
        
        return node

    }
    
    private func successorOfNode(node: TreeNode<T>) -> TreeNode<T>? {
        if let right = node.rightChild {
            var leftmostChild = right
            while leftmostChild.leftChild != nil {
                leftmostChild = leftmostChild.leftChild!
            }
            return leftmostChild
        } else {
            return nil
        }
    }
    
    private func predecessorOfNode(node: TreeNode<T>) -> TreeNode<T>? {
        if let left = node.leftChild {
            var rightmostChild = left
            while rightmostChild.rightChild != nil {
                rightmostChild = rightmostChild.rightChild!
            }
            return rightmostChild
        } else {
            return nil
        }
    }
    
    
    public func bfsDescription() -> String {
        var q = Queue<(node: TreeNode<T>, level: Int)>()
        var result = ""
        if let root = root {
            q.enqueue(node: root, level: 0)
            while !q.isEmpty() {
                var element = q.dequeue()!
                var node = element.node
                var level = element.level
                result += "\(level): \(node.value) "
                var nextLevel = level + 1
                if let left = node.leftChild {
                    q.enqueue(node: left, level: Int(nextLevel))
                }
                if let right = node.rightChild {
                    q.enqueue(node: right, level: Int(nextLevel))
                }
            }
            return result
            
        }
        return result
    }
    
    
    public func descriptionInOrder() -> String {
        return descriptionInOrderFromNode(root)
    }
    
    private func descriptionInOrderFromNode(node: TreeNode<T>?) -> String {
        var result = ""
        if let node = node {
            result += descriptionInOrderFromNode(node.leftChild)
            result += " \(node.value)"
            result += descriptionInOrderFromNode(node.rightChild)
        }
        
        return result
    }
    
    public func descriptionPreOrder() -> String {
        return ""
    }
    
    public func descriptionPostOrder() -> String {
        return ""
    }
    
    public func heightOfTree() -> Int {
        var height =  heightFromNode(root)
        return height
        
    }
    
    public func heightFromNode(node: TreeNode<T>?) -> Int {
        if node == nil {
            return 0 // start at 0
        } else {
            var height = 1 + max(heightFromNode(node?.leftChild), heightFromNode(node?.rightChild))
            return height
        }
        
    }
    
    
    
}