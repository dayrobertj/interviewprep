//
//  Question1.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/3/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation


//With additional data structures
func stringAllUnique(myString: String) -> Bool {
    let stringLength = myString.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
    var set  = NSMutableSet(capacity: stringLength)

    for char in myString {
        set.addObject("\(char)")
    }
    return set.count == stringLength
}


//Without additional data structures

func stringAllUniqueInline(myString: String) -> Bool {
    let stringLength = myString.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
    var stringArray = Array(myString)
    for var i = 0; i < stringLength - 1; i++ {
        for var j = i + 1; j < stringLength; j++ {
            if stringArray[i] == stringArray[j] {
                return false
            }
        }
    }
    return true
}


func reverseCStyleString(input: String) -> String {
    let stringLength = input.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
    var newString = NSMutableString()
    let stringArray = Array(input)
    for var i = stringLength - 1; i >= 0; i-- {
        newString.appendString("\(stringArray[i])")
    }
    return newString
}

public func stringAreAnagrams(string1: String, string2: String) -> Bool {
    let stringLength1 = string1.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
    let stringLength2 = string2.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
    if stringLength1 != stringLength2 { return false}
    
    
    var charDict = [String:Int]()
    
    for char in string1 {
        if charDict["\(char)"] == nil {
            charDict["\(char)"] = 1
        } else {
            charDict["\(char)"] = charDict["\(char)"]! + 1
        }
    }
    
    for char in string2 {
        if charDict["\(char)"] == nil {
            return false
        } else if charDict["\(char)"] <= 0 {
            return false
        } else {
            charDict["\(char)"] = charDict["\(char)"]! - 1
        }
    }
    return true
}

//Question 1.5
public func encodeSpaces(string: String) -> String {
    let stringLength = string.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
    var stringArray = Array(string)
    var newString = ""
    for var i = 0; i < stringLength; i++ {
        if stringArray[i] == " " {
            newString += "%20"
        } else {
            newString.append(stringArray[i])
        }
    }
    return newString
}

public func specialIsSubstring(string1: String, string2: String) {
    var combined = string1 + string1
//    return isSubstring(combined, string2)
}

//A B C D
//E F G H
//I J K L
//M N O P

//M I E A
//N J F B
//O K G C
//P L H D
//Assume array is n x n


//var currentVal = array[row][col]
//var newRow = col
//var newCol = totalElements - 1 - row
//placeholder = array[newRow][newCol]
//array[newRow][newCol] = currentVal
public func rotateArray90Degrees(inout array: [[String]]){
    var totalElements = array.count
    var totalIndices = totalElements - 1
    for var row = 0; row < totalElements / 2; row++ {
        var placeholder = ""
        for var col = row; col < totalElements - 1 - row; col++ {
//          0,0 | 0,3 | 3,3| 3,0
            var currentVal = array[row][col] //0,0
            var secondVal  = array[col][totalIndices - row] //0,3
            var thirdVal   = array[totalIndices - row][totalIndices - col] //3,3
            var forthVal   = array[totalIndices - col][row] //3,0

            placeholder = array[row][col] //Save first value
            array[row][col] = array[totalIndices - col][row] //Move forth to first
            array[totalIndices - col][row] = array[totalIndices - row][totalIndices - col] //Move third to forth
            array[totalIndices - row][totalIndices - col] = array[col][totalIndices - row] //move second to third
            array[col][totalIndices - row] = placeholder //move first (saved) to second
            println("\(array)")
        }
    }
}