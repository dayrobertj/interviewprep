//
//  Chapter8.m
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "Chapter8.h"
#import "UnsafeQueue.h"

@implementation Chapter8

+ (NSInteger)fibonacciNumberAtIndex:(NSInteger)index {
    if (index <= 1) {
        return 1;
    } else {
        return [Chapter8 fibonacciNumberAtIndex:index - 1] + [Chapter8 fibonacciNumberAtIndex:index - 2];
    }
}
//TODO: 8.2

//TODO: Figure out how to write this recursively
//Write a method that returns all subsets of a set
+ (NSArray *)allSubsetsOfSet:(NSArray *)set {
    NSMutableArray *allSubsets = [[NSMutableArray alloc] init];
    [allSubsets addObject:@[set[0]]];
    for(NSInteger i = 1; i < [set count]; i++) {
        NSNumber *currentValue = (NSNumber *)set[i];
        NSMutableArray *newSubsets = [[NSMutableArray alloc] init];
        [newSubsets addObject:@[currentValue]];
        for(NSArray *subset in allSubsets) {
            NSArray *newSubset = [subset arrayByAddingObject:currentValue];
            [newSubsets addObject:newSubset];
        }
        [allSubsets addObjectsFromArray:newSubsets];
    }
    return allSubsets;
}

- (NSArray *)permutateString:(NSString *)str {
    NSMutableArray *allTerms = [NSMutableArray new];
    if(str.length == 1) {
        [allTerms addObject:str];
        return allTerms;
    }
    
    char firstChar = [str characterAtIndex:0];
    NSString *remainingCharacters = [str substringFromIndex:1];
    NSArray *words = [self permutateString:remainingCharacters];
    for(NSString *word in words) {
        for(NSInteger i = 0; i <= word.length; i++) {
            NSString *baseString = [word substringToIndex:i];
            
            NSString *endString = [word substringFromIndex:i];
            NSString *permutation = [NSString stringWithFormat:@"%@%c%@", baseString, firstChar, endString];
            [allTerms addObject:permutation];
        }
    }
    return allTerms;
    
    
}

- (NSArray *)blah:(NSString *)str{
    char firstChar = [str characterAtIndex:0];
    NSMutableArray *permutations = [NSMutableArray new];
    [permutations addObject:[NSString stringWithFormat:@"%c", firstChar]];
    for (NSInteger i = 1; i < str.length; i++) {
        char currentChar = [str characterAtIndex:i];
        NSMutableArray *newPermutations = [NSMutableArray new];
        for(NSString *permutation in permutations) {
            for(NSInteger j = 0; j <= permutation.length; j++) {
                NSString *baseString = [permutation substringToIndex:j];
                
                NSString *endString = [permutation substringFromIndex:j];
                NSString *permutation = [NSString stringWithFormat:@"%@%c%@", baseString, currentChar, endString];
                [newPermutations addObject:permutation];
            }
        }
        permutations = newPermutations;
    }
    
    return permutations;
}


- (NSArray *)permuteCharacter:(NSString *)permChar ontoStrings:(NSArray *)strings {
    NSMutableArray *permutations = [NSMutableArray new];
    for(NSString *string in strings) {
        for(NSInteger j = 0; j <= string.length; j++) {
            NSString *baseString = [string substringToIndex:j];
            
            NSString *endString = [string substringFromIndex:j];
            NSString *permutation = [NSString stringWithFormat:@"%@%@%@", baseString, permChar, endString];
            [permutations addObject:permutation];
        }
    }
    
    return permutations;
}


- (NSArray *)blah2:(NSString *)str index:(NSInteger)index {
    NSMutableArray *allTerms = [NSMutableArray new];
    if(index == 0) {
         NSString *currentChar = [NSString stringWithFormat:@"%c", [str characterAtIndex:index]];
        [allTerms addObject:currentChar];
        return allTerms;
    }
    NSArray *currentPerms = [self blah2:str index:index - 1];
    NSString *currentChar = [NSString stringWithFormat:@"%c", [str characterAtIndex:index]];
    NSArray *newPerms = [self permuteCharacter:currentChar ontoStrings:currentPerms];
    
    return newPerms;
}



- (NSArray *)allSubsetsOfSet:(NSArray *)set atIndex:(NSInteger)index existingResults:(NSArray *)results  {
 
    if (index == 0) {
        return @[set[0]];
    } else {
        //At any point i, the possible sets is all sets for i - 1 combined with all the sets of i - 1 with i's value appended
        NSInteger currentValue = (NSInteger)set[index];
        NSMutableArray *newSubsets = [[NSMutableArray alloc] init];
        for(NSArray *subset in results) {
            NSArray *newSubset = [subset arrayByAddingObject:@(currentValue)];
            [newSubsets addObject:newSubset];
        }
        return [results arrayByAddingObjectsFromArray:newSubsets];
    }
}


- (NSArray *)allPermutationsForParens:(NSArray *)parens atIndex:(NSInteger)index {
    if(index == 0) {
        return @[parens[index]];
    }
    NSArray *currentParens = [self allPermutationsForParens:parens atIndex:index - 1];
    NSArray *permutations = [self permutateParensWithNewParens:currentParens];
    return permutations;
}


- (NSArray *)permutateParensWithNewParens:(NSArray *)parens {
    NSMutableArray *permutations = [NSMutableArray new];
    NSString *firstPemutation = parens[0];
    NSInteger totalParens = firstPemutation.length / 2;
    NSMutableString *appendPerm = [NSMutableString new];
    for(NSInteger i = 0; i <= totalParens; i++) {
        [appendPerm appendString:@"()"];
    }
    [permutations addObject:appendPerm];
    for(NSString *permutation in parens) {

        for(NSInteger i = 0; i < permutation.length; i++) {
            NSString *currentChar = [NSString stringWithFormat:@"%c", [permutation characterAtIndex:i]];
            if([currentChar isEqualToString:@"("]) {
                NSString *prefix = [permutation substringToIndex:i+1];
                NSString *suffix = [permutation substringFromIndex:i+1];
                NSString *newPermutation = [NSString stringWithFormat:@"%@()%@", prefix, suffix];
                [permutations addObject:newPermutation];
            }
        }
    }
    return permutations;
}


//Implement the “paint fill” function that one might see on many image editing programs
//That is, given a screen (represented by a 2 dimensional array of Colors),
// a point, and a new color, fill in the surrounding area until you hit a border of that color’

- (NSArray *)paintFillWithArray:(NSArray *)grid startingPoint:(NSArray *)startingPoint color:(NSNumber *)color {
    
    NSMutableArray *mutableGrid = [grid mutableCopy];
    NSInteger gridHeight = [grid count];
    NSArray *firstArray = (NSArray *)grid[0];
    NSInteger gridWidth = [firstArray count];
    
    UnsafeQueue *q = [[UnsafeQueue alloc] init];
    
    
    [q enqueueItem:startingPoint];
    while(![q isEmpty]) {
        NSArray *currentPoint = (NSArray *)[q dequeue];
        NSNumber *column = currentPoint[0];
        NSNumber *row = currentPoint[1];
        NSInteger columnInt = column.integerValue;
        NSInteger rowInt = row.integerValue;
        NSMutableArray *rowArray = mutableGrid[rowInt];
        rowArray[columnInt] = color;
        
        
        //Now we need to make four new points that are up, down, left, and right.
        //We will only queue these points if they are not already colored
        
        //Top Point
        if(rowInt - 1 >= 0) {
            NSNumber *newColumn = column;
            NSNumber *newRow = @(rowInt - 1);
            NSArray *topPoint = @[newColumn, newRow];
            NSNumber *currentColor = grid[newRow.integerValue][newColumn.integerValue];
            if(currentColor.integerValue != color.integerValue) {
                [q enqueueItem:topPoint];
            }
        }
        
        //Left Point
        if(columnInt - 1 >= 0) {
            NSNumber *newColumn = @(columnInt - 1);
            NSNumber *newRow = row;
            NSArray *newPoint = @[newColumn, newRow];
            NSNumber *currentColor = grid[newRow.integerValue][newColumn.integerValue];
            if(currentColor.integerValue != color.integerValue) {
                [q enqueueItem:newPoint];
            }
        }
        
        //Bottom Point
        if(rowInt + 1 < gridHeight) {
            NSNumber *newColumn = column;
            NSNumber *newRow = @(rowInt + 1);
            NSArray *newPoint = @[newColumn, newRow];
            NSNumber *currentColor = grid[newRow.integerValue][newColumn.integerValue];
            if(currentColor.integerValue != color.integerValue) {
                [q enqueueItem:newPoint];
            }
        }
        
        //Right Point
        if(columnInt + 1 < gridWidth) {
            NSNumber *newColumn = @(columnInt + 1);
            NSNumber *newRow = row;
            NSArray *newPoint = @[newColumn, newRow];
            NSNumber *currentColor = grid[newRow.integerValue][newColumn.integerValue];
            if(currentColor.integerValue != color.integerValue) {
                [q enqueueItem:newPoint];
            }
        }
        
    }
    return mutableGrid;

}
// n choose k = (n-1 choose k) + (n-1 choose k-1)
//Can use pennies, nickles, dimes, and quarters

NS_ENUM(NSInteger, Coin) {
    CoinQuarter,
    CoinDime,
    CoinNickel,
    CoinPenny
};


//Base case is wrong
-(NSArray *)makeChangeForCents:(NSInteger)cents coinIndex:(NSInteger)index {
    NSArray *coins = @[@1,@5,@10,@25];
    //For any given point, there are a fininite amount of ways to make change
    if(index == 0) {
        return @[[NSString stringWithFormat:@"%ld-1", (long)cents]];
    }
    
    NSInteger currentCoinValue = ((NSNumber *)coins[index]).integerValue;
    NSMutableArray *totalResults = [NSMutableArray array];
    for(NSInteger i = 0; i * currentCoinValue <= cents; i++) {
        NSInteger remainder = cents - i * currentCoinValue;
        
        //Problem here is that it won't be able to merge in this step because no values will be passed back.
        NSArray *permutations = [self makeChangeForCents:remainder coinIndex:index - 1];
        for(NSString *permutation in permutations) {
            NSString *newPermutation = [NSString stringWithFormat:@"%ld-%ld,%@", (long)i, (long)currentCoinValue, permutation];
            [totalResults addObject:newPermutation];
        }
    }
   
    return totalResults;
    
}



@end
