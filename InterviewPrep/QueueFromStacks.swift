//
//  QueueFromStacks.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/19/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class QueueFromStacks<T> {
    var stack1 = Stack<T>()
    var stack2 = Stack<T>()
    public init() {}
    
    public func enqueue(element: T) {
//        if stack1.count >= 0 {
            stack1.push(element)
//        } else {
//            stack2.push(element)
//        }
    }
    
    public func dequeue() -> T? {
        var returnItem : T?

        var element = stack1.pop()
        while element != nil {
            stack2.push(element!)
            element = stack1.pop()
        }
        returnItem = stack2.pop()
        
        element = stack2.pop()
        while element != nil {
            stack1.push(element!)
            element = stack2.pop()
        }
    
        return returnItem
    }
    
    
    
}