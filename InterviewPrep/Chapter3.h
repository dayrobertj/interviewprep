//
//  Chapter3.h
//  InterviewPrep
//
//  Created by Robert Day on 3/9/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Chapter3 : NSObject

- (NSNumber *)setAllBitsInN:(NSNumber *)N toM:(NSNumber *)M betweenIndexI:(NSInteger)i andJ:(NSInteger)j;
- (void)printNextBinarySmallestAndLargestOfNumber:(NSNumber *)number;
- (NSNumber *)swapEvenAndOnBitsInNumber:(NSNumber *)number;
@end
