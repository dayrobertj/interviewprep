//
//  main.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

//println("Hello, World!")
//
//var isUnique = stringAllUnique("HELLO")
//println("Is string unique \(isUnique)")
//
//var isUnique2 = stringAllUnique("HELO")
//println("Is string unique \(isUnique2)")
//
//
//var isUnique3 = stringAllUniqueInline("HELLO")
//println("Is string unique \(isUnique3)")
//
//var isUnique4 = stringAllUniqueInline("HELO")
//println("Is string unique \(isUnique4)")
//
//var reversedString = reverseCStyleString("I Like Bananas")
//println("Reversed is: \(reversedString)")
//
//var isAnagram = stringAreAnagrams("THE EYES", "THEY SEE")
//println("Is anagram \(isAnagram)")
//
//var isAnagram2 = stringAreAnagrams("THE EYES1", "THEY SEE")
//println("Is anagram \(isAnagram2)")

func testQueue() {
    var myQueue = Queue<Int>()
    myQueue.enqueue(5)
    myQueue.enqueue(10)
    myQueue.enqueue(11)
    
    println(myQueue.dequeue())
    println(myQueue.dequeue())
    println(myQueue.dequeue())
    println(myQueue.dequeue())
}

//testQueue()


func testStack() {
    var myStack = Stack<Int>()
    
    myStack.push(10)
    myStack.push(20)
    myStack.push(1111)
    
    println("Stack pop: \(myStack.pop())")
    println("Stack pop: \(myStack.pop())")
    println("Stack pop: \(myStack.pop())")
}

//testStack()


func testLinkedList() {
    var myList = LinkedList()
    var node1 = Node()
    node1.value = "HELLO"
    
    var node2 = Node()
    node2.value = "Node 2"
    
    var node3 = Node()
    node3.value = "Node 3"
    
    
    myList.addNode(node1)
    myList.addNode(node2)
    myList.addNode(node3)
    
    
    println(myList.firstNode!.next.value)
    
}

//testLinkedList()



func testMergesort() {
    var input = [9,5,2,0,-10,100,50,4]
    mergeSort(&input)
    println("Sorted is \(input)")
}

//testMergesort()