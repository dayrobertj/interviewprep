//
//  Chapter9.h
//  InterviewPrep
//
//  Created by Robert Day on 3/12/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property(nonatomic) NSInteger height;
@property(nonatomic) NSInteger weight;
-(instancetype)initWithHeight:(NSInteger)height weight:(NSInteger)weight;
@end

@interface Chapter9 : NSObject

- (void)mergeTwoArraysWithHoldingArray:(NSMutableArray *)holdingArray toMergeArray:(NSArray *)arrayToMerge;
- (NSInteger)findNumber:(NSInteger)number inSortedAndShiftedArray:(NSArray *)array;
- (NSInteger)findIndexOfString:(NSString *)string inArray:(NSArray *)array;
- (CGPoint)findElement:(NSInteger)element inMatrix:(NSArray *)matrix;
-(NSArray *)bestCircusTowerWithPerformers:(NSArray *)performers;
@end
