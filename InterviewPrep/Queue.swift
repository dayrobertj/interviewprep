//
//  Queue.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class Queue<T> {

    private var items = [T]()
    
    func enqueue(item : T) {
        items.append(item)
    }
    
    func dequeue() -> T? {
        if items.count == 0 {
            return nil
        } else {
            return items.removeAtIndex(0)
        }
    }
    
    public func isEmpty() -> Bool {
        return items.count == 0
    }
}