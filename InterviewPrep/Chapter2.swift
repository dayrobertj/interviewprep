//
//  Chapter2.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/4/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation


public func removeDuplicatesFromLinkedList(list: LinkedList) {
    var mySet = NSMutableSet()
    var previousNode : ListNode!
    var currentNode = list.firstNode
    while currentNode != nil {
        println(currentNode!.value)
        if mySet.containsObject(currentNode!.value) {
            println("Previous: \(previousNode.value)")
            println("Next: \(currentNode!.next.value)")
            previousNode.next = currentNode!.next
        } else {

            mySet.addObject(currentNode!.value)
            //Only override the previous node if we wanted to link to the current
            previousNode = currentNode!
        }

        currentNode = currentNode!.next
    }


}


public func findNthLastElementInLinkedList(list: LinkedList, nthLastElement: Int) -> ListNode? {
    var stack = Stack<ListNode>()
    var currentNode = list.firstNode
    while currentNode != nil {
        stack.push(currentNode!)
        currentNode = currentNode!.next
    }
    
    var returnNode : ListNode?
    for var i = 0; i < nthLastElement; i++ {
        returnNode = stack.pop()
    }
    return returnNode
}

public func deleteNodeFromList(node: ListNode) {
    node.value = node.next.value
    node.next = node.next.next
}

//You have two numbers represented by a linked list, where each node contains a sin- gle digit The digits are stored in reverse order, such that the 1’s digit is at the head of the list Write a function that adds the two numbers and returns the sum as a linked list

public func sumifyLinkedList(list1: LinkedList, list2: LinkedList) -> LinkedList {
    var firstVal = 0
    var secondVal = 0
    
    var currentNode = list1.firstNode
    var currentDepth = 1
    while currentNode != nil {
        var intValue = currentNode!.value.integerValue
        firstVal += intValue * currentDepth
        currentDepth *= 10
        currentNode = currentNode!.next
    }
    
    currentDepth = 1
    currentNode = list2.firstNode
    while currentNode != nil {
        var intValue = currentNode!.value.integerValue
        secondVal += intValue * currentDepth
        currentDepth *= 10
        currentNode = currentNode!.next
    }
    
    var result = firstVal + secondVal
    
    var resultList = LinkedList()
    while result > 0 {
        var val = result % 10
        result = result / 10
        resultList.prependNode(ListNode(value:val))
    }
    
    return resultList
}

public func sortStackAscending(stack: Stack<Int>) {
    var stackElements = [Int]()
    var currentElement = stack.pop()
    while currentElement != nil {
        stackElements.append(currentElement!)
        currentElement = stack.pop()
    }
    
    mergeSort(&stackElements)
    
    for var i = 0; i < stackElements.count; i++ {
        stack.push(stackElements[i])
    }
}
