//
//  QuickSort.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class QuickSort {
    public init() {}
    public func QuickSort(inout input: [Int]) {
        self.quickSort(&input, p: 0, r: input.count - 1)
    }
    
    func quickSort(inout input: [Int], p: Int, r: Int) {
        if p < r {
            var partitionIndex = self.partition(&input, p: p, r: r)
            self.quickSort(&input, p: p, r: partitionIndex - 1)
            self.quickSort(&input, p: partitionIndex + 1, r: r)
        }
    }
    
    private func partition(inout input : [Int], p: Int, r: Int) -> Int {
        var pivotIndex = r
        var pivotValue = input[pivotIndex]
        
        var highIndex = p
        //highIndex is a rolling window that moves one place to the right every time a value smaller than the pivot is found
        for var i = p; i <= r - 1; i++ {
            if input[i] < pivotValue {
                //if the value is smaller than the pivot, swap the value with the current high / low barrier (highIndex), bump highIndex
                var placeholder = input[i]
                input[i] = input[highIndex]
                input[highIndex] = placeholder
                highIndex++
            }
        }
        //Put the pivot into place
                println("INput here: \(input)")
        var placeholder = input[highIndex]
        input[highIndex] = input[r]
        input[r] = placeholder
        return highIndex
        
    }
}



