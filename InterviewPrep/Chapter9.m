//
//  Chapter9.m
//  InterviewPrep
//
//  Created by Robert Day on 3/12/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "Chapter9.h"
@implementation Person
-(instancetype)initWithHeight:(NSInteger)height weight:(NSInteger)weight {
    self = [super init];
    if(self) {
        _weight = weight;
        _height = height;
    }
    return self;
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Height: %ld, Weight: %ld", (long)self.height, (long)self.weight];
}
@end


@implementation Chapter9

//
//-(instancetype)init {
//    self = [super init];
//    return self;
//}
//You are given two sorted arrays, A and B, and A has a large enough buffer at the end to hold B Write a method to merge B into A in sorted order

- (void)mergeTwoArraysWithHoldingArray:(NSMutableArray *)holdingArray toMergeArray:(NSArray *)arrayToMerge {

    NSInteger j = [arrayToMerge count] - 1;
    
    //Starting point should be the item before the null values
    NSInteger i = [holdingArray count] - 1 - (j + 1);
    
    //Pointer where each item should be placed
    NSInteger p = [holdingArray count] - 1;
    
    while (i >= 0 && j >= 0) {
        if(holdingArray[i] > arrayToMerge[j]) {
            holdingArray[p] = holdingArray[i];
            i--;
        } else {
            holdingArray[p] = arrayToMerge[j];
            j--;
        }
        p--;
    }
    while(i >= 0) {
        holdingArray[p] = holdingArray[i];
        i--;
        p--;
    }
    
    while(j >= 0) {
        holdingArray[p] = arrayToMerge[i];
        i--;
        p--;
    }
}
//TODO: Maybe come back
//Write a method to sort an array of strings so that all the anagrams are next to each other

//Given a sorted array of n integers that has been rotated an unknown number of times,giveanO(logn)algorithm that finds an element in the array You may assume that the array was originally sorted in increasing order
//EXAMPLE:
//Input: find 5 in array (15 16 19 20 25 1 3 4 5 7 10 14)
//Output: 8 (the index of 5 in the array)

//5 6 7 8 9 10 11 1 2 3 4
//
//
//
//Know starting number is 5, search for 3
//hit 10, if number < 10 && number >= startingNumber
//go left
//else
//go right


- (NSInteger)findNumber:(NSInteger)number inSortedAndShiftedArray:(NSArray *)array {
    if(array.count == 0) return -1;
    NSInteger firstNumber = [array[0] integerValue];
    if(firstNumber == number) {
        return 0;
    }

    return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:0 endingIndex:[array count] -1];
    
}
//Number < value && > starting
//Go left
//
//number > value && > starting
//go right

//number < value && number < endpoint
//go left
//
//number > value && number < endpoint
//go right
//5 6 7 8 9 10 11 1 2 3 4
- (NSInteger)findNumber:(NSInteger)number inSortedAndShiftedArray:(NSArray *)array withStartingIndex:(NSInteger)startingIndex endingIndex:(NSInteger)endingIndex {
    NSInteger midPoint = (startingIndex + endingIndex) / 2;
    if(startingIndex > endingIndex) {
        return -1;
    }
    NSInteger valueAtMidpoint = [array[midPoint] integerValue];
    NSInteger startingValue = [array[0] integerValue];
    NSInteger endingValue = [array[array.count - 1] integerValue];
    if(valueAtMidpoint == number) return midPoint;
    
    if(valueAtMidpoint > startingValue) {
        if(number > valueAtMidpoint) {
            return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:midPoint + 1 endingIndex:endingIndex];
        } else if(number < startingValue) {
            return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:midPoint + 1 endingIndex:endingIndex];
        } else {
            return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:0 endingIndex:midPoint];
        }
    } else {
        if (number < valueAtMidpoint) {
            return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:0 endingIndex:midPoint];
        } else if (number > endingValue) {
            return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:0 endingIndex:midPoint];
        } else {
            return [self findNumber:number inSortedAndShiftedArray:array withStartingIndex:midPoint + 1 endingIndex:endingIndex];
        }
    }    
    
}

//Given a sorted array of strings which is interspersed with empty strings, write a method to find the location of a given string
//find “ball” in [“at”, “”, “”, “”, “ball”, “”, “”, “car”, “”, “”, “dad”, “”, “”] will return 4

//STrategy: Find the midpoint, if no quote, binary search, else: walk until find a non quote, binary search
- (NSInteger)findIndexOfString:(NSString *)string inArray:(NSArray *)array {
    return [self findIndexOfString:string inArray:array startingIndex:0 endingIndex:array.count - 1];
}

- (NSInteger)findIndexOfString:(NSString *)string inArray:(NSArray *)array startingIndex:(NSInteger)startingIndex endingIndex:(NSInteger)endingindex {
    if (startingIndex > endingindex) {
        return -1;
    }
    NSInteger midIndex = (startingIndex + endingindex) / 2;
    NSInteger lookupIndex = midIndex;
    NSString *midValue = array[midIndex];
    while ([midValue isEqualToString:@""] && lookupIndex < endingindex) {
        lookupIndex++;
        midValue =array[lookupIndex];
    }
    if([midValue isEqualToString:@""]) {
        lookupIndex = midIndex;
        midValue = array[lookupIndex];
        while ([midValue isEqualToString:@""] && lookupIndex > startingIndex) {
            lookupIndex--;
            midValue = array[lookupIndex];
        }
    }
    if([midValue isEqualToString:@""]) {
        return -1;
    }
    if([midValue isEqualToString:string]) {
        return lookupIndex;
    } else if([string isLessThan:midValue]) {
        return [self findIndexOfString:string inArray:array startingIndex:startingIndex endingIndex:lookupIndex-1];
    } else {
        return [self findIndexOfString:string inArray:array startingIndex:lookupIndex+1 endingIndex:endingindex];
    }

    
}
//Assumptions
//» Rows are sorted left to right in ascending order Columns are sorted top to bottom in ascending order
//» Matrix is of size MxN
// Given a matrix in which each row and each column is sorted, write a method to find an element in it
// Example
//00 10 20 30 40
//05 11 21 31 41
//10 12 22 32 42
//15 16 23 33 43
//20 21 24 34 44

- (CGPoint)findElement:(NSInteger)element inMatrix:(NSArray *)matrix {
    if(matrix.count == 0) {
        return CGPointZero;
    }
    NSInteger rows = matrix.count;
    NSInteger cols = [(NSArray *)matrix[0] count] - 1;
    
    NSInteger rowIndex = 0;
    NSInteger colIndex = cols;
    while(rowIndex < rows && colIndex >= 0) {
        NSInteger val = [(NSNumber *)matrix[rowIndex][colIndex] integerValue];
        if(val == element) {
            return CGPointMake(rowIndex, colIndex);
        } else if(val > element && rowIndex == 0) {
            colIndex--;
        } else {
            if(rowIndex == rows - 1) {
                colIndex--;
                rowIndex = 0;
            } else {
                rowIndex++;
            }
        }
    }
    return CGPointZero;
    
    
}

//EXAMPLE:
//Input (ht, wt): (65, 100) (70, 150) (56, 90) (75, 190) (60, 95) (68, 110)
//Output: The longest tower is length 6 and includes from top to bottom: (56, 90) (60,95) (65,100) (68,110) (70,150) (75,190)

-(NSArray *)bestCircusTowerWithPerformers:(NSArray *)performers {
    NSArray *sortedByWeight = [performers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Person *person1 = (Person *)obj1;
        Person *person2 = (Person *)obj2;
        return person1.weight > person2.weight;
    }];
    
    NSArray *sortedByHeight = [performers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Person *person1 = (Person *)obj1;
        Person *person2 = (Person *)obj2;
        return person1.height > person2.height;
    }];
    
    NSArray *maxByWeight = [self maxArrayByWeightWithArray:sortedByWeight];
    NSArray *maxByHeight = [self maxArrayByHeightWithArray:sortedByHeight atIndex:0];
    
    if(maxByHeight.count > maxByWeight.count) {
        return maxByHeight;
    } else {
        return maxByWeight;
    }
    
    
}
//Example: (56, 150), (60, 160)  (68, 120)(70, 140)  (75, 145) (78, 100)
//         (78, 100)  (68, 120) (70, 140)  (75, 145) (56, 150) (60, 160)
-(NSArray *)maxArrayByHeightWithArray:(NSArray *)sortedArray atIndex:(NSInteger)index {
    
    NSMutableArray *weights = [NSMutableArray new];
    for(NSInteger i = 0; i < sortedArray.count; i++) {
        [weights addObject:@0];
    }
    //At any point, the best possible combination is either:
        //The previous possible combination + current element
        //The current element
    weights[0] = @(1);
    NSInteger maxIndex = 0;
    for(NSInteger i = 1; i < sortedArray.count; i++) {
        NSNumber *previousWeight = @([(Person *)sortedArray[i - 1] weight]);
        NSNumber *currentWeight = @([(Person *)sortedArray[i] weight]);
        if(currentWeight.integerValue >= previousWeight.integerValue) {
            weights[i] = @([(NSNumber *)weights[i-1] integerValue] + 1);
            if([weights[i] integerValue] > [weights[maxIndex] integerValue]) {
                maxIndex = i;
            }
        } else {
            weights[i] = @(1);
        }
    }
    //At this point, we have an array containing the maximum people in a row from any given point
    //Let's backtrack to find the longest contiguous list
    NSInteger maxInARow = [weights[maxIndex] integerValue];
    NSMutableArray *results = [NSMutableArray new];
    
    while(maxInARow > 0) {
        [results addObject:sortedArray[maxIndex]];
        maxInARow--;
        maxIndex--;
    }
    return [results copy];
}

-(NSArray *)maxArrayByWeightWithArray:(NSArray *)sortedArray {
    
    NSMutableArray *heights = [NSMutableArray new];
    for(NSInteger i = 0; i < sortedArray.count; i++) {
        [heights addObject:@0];
    }
    //At any point, the best possible combination is either:
    //The previous possible combination + current element
    //The current element
    heights[0] = @(1);
    NSInteger maxIndex = 0;
    for(NSInteger i = 1; i < sortedArray.count; i++) {
        NSNumber *previousHeight = @([(Person *)sortedArray[i - 1] height]);
        NSNumber *currentHeight = @([(Person *)sortedArray[i] height]);
        if(currentHeight.integerValue >= previousHeight.integerValue) {
            heights[i] = @([(NSNumber *)heights[i-1] integerValue] + 1);
            if([heights[i] integerValue] > [heights[maxIndex] integerValue]) {
                maxIndex = i;
            }
        } else {
            heights[i] = @(1);
        }
    }
    //At this point, we have an array containing the maximum people in a row from any given point
    //Let's backtrack to find the longest contiguous list
    NSInteger maxInARow = [heights[maxIndex] integerValue];
    NSMutableArray *results = [NSMutableArray new];
    
    while(maxInARow > 0) {
        [results addObject:sortedArray[maxIndex]];
        maxInARow--;
        maxIndex--;
    }
    return [results copy];
}
@end








