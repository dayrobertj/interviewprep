//
//  Chapter3.m
//  InterviewPrep
//
//  Created by Robert Day on 3/9/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "Chapter3.h"
#import "NSInteger+Binary.h"

@implementation Chapter3

//Input: N = 10000000000, M = 10101, i = 2, j = 6
//Output: N = 10001010100


- (instancetype)init {
    self = [super init];
    return self;
}

//Expected result: 10001010100
- (NSNumber *)setAllBitsInN:(NSNumber *)N toM:(NSNumber *)M betweenIndexI:(NSInteger)i andJ:(NSInteger)j {
    NSInteger mask = (1 << (j - i + 1)) - 1;
    NSLog(@"first mask mask is: %@", [@(mask) binaryRespresentation]);
    //Mask should now be 00000000 00011111
    mask = mask << i;
    //Mask should now be 00000000 01111100
    NSLog(@"second mask mask is: %@", [@(mask) binaryRespresentation]);
    mask = ~mask;
    //Mask should now be 11111111 10000011
    NSLog(@"third mask mask is: %@", [@(mask) binaryRespresentation]);

    NSLog(@"Binary N: %@", [N binaryRespresentation]);
    //NewN has to have 1's for all possible digits left of th emast
    NSNumber *newN = @(N.integerValue & mask);
    NSLog(@"newN is: %@", [newN binaryRespresentation]);
    
    NSNumber *newM = @(M.integerValue << i);
    NSLog(@"newM is: %@", [newM binaryRespresentation]);
    NSInteger result = newN.integerValue ^ newM.integerValue;
    NSNumber *fullResult = @(result);
    NSLog(@"binary result is: %@", [fullResult binaryRespresentation]);
    return @(result);
}

- (NSInteger)count1sInBinaryNumber:(NSNumber *)number {
    NSInteger totalOnes = 0;
//    NSLog(@"Number is: %@", [number binaryRespresentation]);
    NSInteger intNumber = number.integerValue;
    while(intNumber > 0) {
        NSInteger nextTerm = intNumber & 1;
        if(nextTerm == 1) {
            totalOnes++;
        }
        intNumber = intNumber >> 1;
    }
    return totalOnes;
}

//Given an integer, print the next smallest and next largest number that have the same number of 1 bits in their binary representation
- (void)printNextBinarySmallestAndLargestOfNumber:(NSNumber *)number {
    //Get smaller number
    NSInteger startingTotalOnes = [self count1sInBinaryNumber:number];
    
    NSInteger smallerNumber = number.integerValue - 1;
    NSInteger countSmallerNumber = [self count1sInBinaryNumber:@(smallerNumber)];
    while(countSmallerNumber != startingTotalOnes && countSmallerNumber > 0) {
        smallerNumber = smallerNumber - 1;
        countSmallerNumber = [self count1sInBinaryNumber:@(smallerNumber)];
    }
    if(smallerNumber == 0) {
        NSLog(@"WE HIT 0, a special case.");
    } else {
        NSLog(@"Smaller number is: %ld", (long)smallerNumber);
    }
    
    NSInteger largerNumber = number.integerValue + 1;
    NSInteger countLargerNumber = [self count1sInBinaryNumber:@(largerNumber)];
    while(countLargerNumber != startingTotalOnes && largerNumber < NSIntegerMax) {
        largerNumber += 1;
        countLargerNumber = [self count1sInBinaryNumber:@(largerNumber)];
    }
    
    if(largerNumber == NSIntegerMax) {
        NSLog(@"We hit the max value");
    } else {
        NSLog(@"Next largest number is %ld", (long)largerNumber);
    }
    
}

//Write a program to swap odd and even bits in an integer with as few instructions as possible (e g , bit 0 and bit 1 are swapped, bit 2 and bit 3 are swapped, etc)
- (NSNumber *)swapEvenAndOnBitsInNumber:(NSNumber *)number {
    //Consider input:  10101010
    //Need to produce: 01010101
    //Need to XOR the input with 1's from the most significant 1 down
    NSLog(@"Input: %@", [number binaryRespresentation]);
    NSInteger totalSigDigits = 0;
    NSInteger intNumber = number.integerValue;
    while(intNumber > 0) {
        totalSigDigits++;
        intNumber = intNumber >> 1;
    }
    NSInteger mask = ((1 << totalSigDigits)) - 1;
    NSInteger output = number.integerValue ^ mask;
    NSNumber *retVal = @(output);

    NSLog(@"Output: %@", [retVal binaryRespresentation]);
    return retVal;
    
}

// An array A[1 n] contains all the integers from 0 to n except for one number which is missing In this problem, we cannot access an entire integer in A with a single opera- tion The elements of A are represented in binary, and the only operation we can use to access them is “fetch the jth bit of A[i]”, which takes constant time Write code to find the missing integer Can you do it in O(n) time?

//Solution, check the 1's bit for each term.  When you see two 1's in a row, the missing number is 1 less than the integer with the second 1
//POOP, that only works with a sorted array

//Actual solution: Count occurences of 1's and 0's in the least signifcant bit.  If there are more 0's, drop all the numbers ending in 0.  Else, drop ones ending in 1.

//Go through future bits and do the same until you're left with 1 number.  The missing value is that number + 1
@end
