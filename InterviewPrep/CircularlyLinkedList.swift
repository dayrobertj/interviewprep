//
//  CircularlyLinkedList.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/4/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class CircularlyLinkedList : LinkedList {
    var lastNode : ListNode?
    public override func appendNode(node: ListNode) {
        super.appendNode(node)
        node.next = self.firstNode
        lastNode = node
    }
    
    public override func prependNode(node: ListNode) {
        super.prependNode(node)
        lastNode!.next = node
    }
}