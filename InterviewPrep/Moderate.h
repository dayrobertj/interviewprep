//
//  Moderate.h
//  InterviewPrep
//
//  Created by Robert Day on 3/14/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Moderate : NSObject
- (BOOL)someoneWonTicTacToeWithBoard:(NSArray *)board;
@end
