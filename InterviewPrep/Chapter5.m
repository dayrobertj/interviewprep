//
//  Chapter5.m
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "Chapter5.h"

@implementation Chapter5
//TODO: Maybe come back to bit manipulation?
//Input: N = 10000000000, M = 10101, i = 2, j = 6
//Output: N = 10001010100
- (NSInteger)setN:(NSInteger)n toM:(NSInteger)m fromI:(NSInteger)i toJ:(NSInteger)j {
    // pad the values found above by shifting bits to the proper position in N
    // 0 the values in N for the range
    // Or the shifted bits onto N
    
    NSInteger paddedM = m << i;
    
    return n;
}


@end
