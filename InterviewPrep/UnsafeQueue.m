//
//  UnsafeQueue.m
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "UnsafeQueue.h"
@interface UnsafeQueue ()
@property (nonatomic, strong) NSMutableArray *items;
@end

@implementation UnsafeQueue

- (instancetype)init {
    self = [super init];
    if(self) {
        _items = [[NSMutableArray alloc] init];
    }
    return self;
}

- (BOOL)isEmpty {
    return self.items.count == 0;
}

- (id)dequeue {
    if (self.items.count == 0) {
        return nil;
    } else {

        id item = self.items[0];
        [self.items removeObjectAtIndex:0];
        return item;
    }
}

- (void)enqueueItem:(id)item {
    [self.items addObject:item];
}
@end
