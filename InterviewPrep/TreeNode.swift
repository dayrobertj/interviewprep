//
//  TreeNode.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/5/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class TreeNode<T> : Printable {
    public var leftChild : TreeNode<T>?
    public var rightChild : TreeNode<T>?
    public var parent : TreeNode<T>?
    public var value : T
    
    public init(value: T) {
        self.value = value
    }
    
    public var description : String {
        return "Value: \(value) LeftValue: \(leftChild?.value) RightValue \(rightChild?.value)"
    }
}