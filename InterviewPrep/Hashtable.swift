//
//  Hashtable.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/22/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation


private class HashEntry<K: Hashable, T> {
    var key: K
    var value: T
    
    init(key: K, value: T) {
        self.key = key
        self.value = value
    }
}


var totalBuckets : Int = 4
public class HashTable<K: Hashable, T> {

    public init() {}
    var currentBucketCount = totalBuckets
    //Could consider this an array of linked lists, really
    private var items : [[HashEntry<K, T>]] = Array(count: totalBuckets, repeatedValue: Array<HashEntry<K, T>>())
    private var itemCount = 0
    public func objectForKey(key: K) -> T? {
        var bucket = bucketForKey(key.hashValue)
        for element in items[bucket] {
            if element.key == key {
                return element.value
            }
        }
        return nil
    }
    
    public func addObject(object: T, forKey key: K) {
        resizeIfNeeded()
        self.addObject(object, forKey: key, intoArray: &items)
        itemCount++
    }
    
    public func removeObjectWithKey(key: K) -> T? {
        var bucket = bucketForKey(key.hashValue)
        var index = 0
        for element in items[bucket] {
            if element.key == key {
                itemCount--
                items[bucket].removeAtIndex(index)
                return element.value
            }
            index++
        }
        return nil
    }
    
    private func bucketForKey(key: Int) -> Int {
        return abs(key) % currentBucketCount
    }
    
    private func addObject(object: T, forKey key: K, inout intoArray array: [[HashEntry<K, T>]]) {
        var bucket = bucketForKey(key.hashValue)
        array[bucket].append(HashEntry(key: key, value: object))
    }
    
    private func resizeIfNeeded() {
        if itemCount  > currentBucketCount * 2 / 3 {
            currentBucketCount*=2
            var newItems = Array(count: currentBucketCount, repeatedValue: Array<HashEntry<K, T>>())

            for bucket in items {
                for hashEntry in bucket {
                    self.addObject(hashEntry.value, forKey: hashEntry.key, intoArray: &newItems)
                }
            }
            items = newItems
        }
    }
    
}