//
//  Heap.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/22/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class MinHeap<T: Comparable> {
    
    private var items = [T]()
    public init() {
        
    }
    
    public func insert(item: T) {
        items.append(item)
        bubbleUpFromIndex(items.count - 1)
    }
    
    
    public func extractMin() -> T? {
        if items.count == 0 {
            return nil
        }
        var min = items[0]
        items[0] = items[items.count - 1]
        items.removeLast()
        bubbleDownFromIndex(0)
        return min
    }
    
    
    //Innefficient method of min heapifying
    public func buildMinHeap(itemsToHeapify: [T]) {
        items = itemsToHeapify
        for var i = items.count / 2; i >= 0; i-- {
            bubbleDownFromIndex(i)
        }

    }
    
    public func treeDescription() -> String {
        return "\(items)"
    }
    
    private func bubbleUpFromIndex(index: Int) {
        if let parentIndex = indexOfParentOfItemAtIndex(index) {
            var currentValue = items[index]
            var parentValue = items[parentIndex]
            println("Current: \(currentValue); parent: \(parentValue)")
            if currentValue < parentValue {
                items[parentIndex] = currentValue
                items[index] = parentValue
                bubbleUpFromIndex(parentIndex)
            }
        }
        
    }
    
    private func bubbleDownFromIndex(index: Int) {
        if items.count == 0 {return}
        var currentValue = items[index]
        var leftIndex = indexOfLeftChildOfItemAtIndex(index)
        var rightIndex = indexOfRightChildOfItemAtIndex(index)
        //Three cases.  No children, 1 child, 2 children
        if leftIndex == nil && rightIndex == nil {
            //No op
        } else if leftIndex != nil && rightIndex != nil {
            var leftValue = items[leftIndex!]
            var rightValue = items[rightIndex!]
            if leftValue < rightValue {
                if currentValue > leftValue {
                    items[leftIndex!] = currentValue
                    items[index] = leftValue
                    //This bubble down will do nothing as there will be no more children
                    bubbleDownFromIndex(leftIndex!)
                }
            } else {
                if currentValue > rightValue {
                    items[rightIndex!] = currentValue
                    items[index] = rightValue
                    //This bubble down will do nothing as there will be no more children
                    bubbleDownFromIndex(rightIndex!)
                }
            }
            
        } else if leftIndex != nil {
            var leftValue = items[leftIndex!]
            if currentValue > leftValue {
                items[leftIndex!] = currentValue
                items[index] = leftValue
                //This bubble down will do nothing as there will be no more children
                bubbleDownFromIndex(leftIndex!)
            }
        } else {
            var rightValue = items[rightIndex!]
            if currentValue > rightValue {
                items[rightIndex!] = currentValue
                items[index] = rightValue
                //This bubble down will do nothing as there will be no more children
                bubbleDownFromIndex(rightIndex!)
            }
        }
    }
    
    private func indexOfParentOfItemAtIndex(index: Int) -> Int? {
        if index == 0 {
            return nil
        } else {
            return (index - 1) / 2
        }
    }
    
    private func indexOfLeftChildOfItemAtIndex(index: Int) -> Int? {
        var leftIndex = index * 2 + 1
        if leftIndex < items.count {
            return leftIndex
        }
        return nil
    }

    private func indexOfRightChildOfItemAtIndex(index: Int) -> Int? {
        var rightIndex = index * 2 + 2
        if rightIndex < items.count {
            return rightIndex
        }
        return nil
    }
    
}