//
//  Stack.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class Stack<T> {
    private var items = [T]()
    public var count : Int {
        return items.count
    }
    
    public func push(item: T) {
        items.append(item)
    }
    
    public func pop() -> T? {
        if items.count == 0 {
            return nil
        } else {
            return items.removeLast()
        }
    }
    public init() {}
}
