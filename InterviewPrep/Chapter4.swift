//
//  Chapter4.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/7/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation


public func isTreeBalanced<T>(tree: BinarySearchTree<T>) -> Bool {
    var leftHeight = tree.heightFromNode(tree.root?.leftChild) ?? 0
    var rightHeight = tree.heightFromNode(tree.root?.rightChild) ?? 0
    
    var diff : Int = leftHeight - rightHeight

    return abs(diff) <= 1
}

public func isPathBetweenTwoNodes() {
    
}

private func balancedTreeFromSortedArray(array: [Int], p: Int, r: Int, tree: BinarySearchTree<Int>) {
    if r > p {
        var midpoint = (p + r) / 2
        var inputValue = array[midpoint]
        tree.insertNode(TreeNode<Int>(value:inputValue))
        balancedTreeFromSortedArray(array, p, midpoint, tree)
        balancedTreeFromSortedArray(array, midpoint + 1, r, tree)
    }

}

public func balancedTreeFromSortedArray(array: [Int]) -> BinarySearchTree<Int> {
    let myTree = BinarySearchTree<Int>()
    balancedTreeFromSortedArray(array, 0, array.count - 1, myTree)
    return myTree
}

public func linkedListsFromTreeLevels(tree: BinarySearchTree<Int>) -> [LinkedList] {
    //Had to do this since standard tuples were being evil
    struct brokenTuple {
        var level : Int
        var node : TreeNode<Int>
        init(level: Int, node: TreeNode<Int>) {
            self.level = level
            self.node = node
        }
    }
    //BFS where it adds each LEVEL to it's own linkedList
    var lists = [LinkedList]()
    var heightOfTree = tree.heightOfTree()
    //Let's create a linked list for every level of the tree
    for var i = 0; i < heightOfTree; i++ {
        lists.append(LinkedList())
    }
    var q = Queue<brokenTuple>()
    var firstNode = tree.root!
    q.enqueue(brokenTuple(level: 0, node: firstNode))
    while !q.isEmpty() {
        var element = q.dequeue()!
        var node = element.node
        var level = element.level
        var listAtLevel : LinkedList = lists[level]
        listAtLevel.appendNode(ListNode(value: node))
        if node.leftChild != nil {
            q.enqueue(brokenTuple(level: level + 1, node: node.leftChild!))
        }
        
        if node.rightChild != nil {
            q.enqueue(brokenTuple(level: level + 1, node: node.rightChild!))
        }
    }
    
    return lists
}


public func commonAncestorOfNodes(tree: BinarySearchTree<Int>, node1: TreeNode<Int>, node2: TreeNode<Int>) -> TreeNode<Int>? {
    
    //Three cases
    // 1) node1 < root && node2 > root
    // 2) Node 1 < root && node2 < root
    // 3) node 2 > root && node2 > root
    // 4) one of the nodes is an ancestor of the other
    //Target: First node where the nodes are on different subtrees
    println("Tree root is \(tree.root)")
    return commonAncestorOfNodes(tree, tree.root, node1, node2)
    
}

//IF this wasn't a binary search tree (WHICH IT WASNT SUPPOSED TO BE)
    //Search the left side & right side.  If 1 element found on each, current node is ancestor
        //if only 1 node is found, and we did this top down, one element isn't in the tree
        //if they are both found on 1 side, call this routine again on that one side

//TODO: what is the running time if this wasn't a binary search tree? We'd need to use search over and over again from the top down. 
    //Would running time be n^2?
private func commonAncestorOfNodes(tree: BinarySearchTree<Int>, startingNode: TreeNode<Int>?, node1: TreeNode<Int>, node2: TreeNode<Int>) -> TreeNode<Int>? {
    
    //Problem: When 1 node isn't in the tree, shit goes bad
    if let  startingNode = startingNode {
        if startingNode.value == node1.value {
            var isFound =  tree.searchNodeWithValue(node2.value)
            if isFound != nil {
                return startingNode
            } else {
                return nil
            }
        } else if startingNode.value == node2.value {
            var isFound =  tree.searchNodeWithValue(node1.value)
            if isFound != nil {
                return startingNode
            } else {
                return nil
            }
        } else if node1.value < startingNode.value && node2.value < startingNode.value {
            return commonAncestorOfNodes(tree, startingNode.leftChild, node1, node2)
        } else if node1.value > startingNode.value && node2.value > startingNode.value {
            return commonAncestorOfNodes(tree, startingNode.rightChild, node1, node2)
        } else {
            //One node is on the left of startingNode and 1 is on the right 
            //This also assumes both are found in the treee
            return startingNode
        }
    } else {
        return nil
    }
}

//You have two very large binary trees: T1, with millions of nodes, and T2, with hun- dreds of nodes Create an algorithm to decide if T2 is a subtree of T1



//This currently also makes an assumption that our values are unique in the parent tree.  If they aren't, the search function would need to be generalized to return any node with a value instead of the first

//These aren't binary search trees but the only different there is the ease of finding a child node
//Need to implement a binary tree that isn't a search tree
public func isTreeSubtree(parentTree: BinaryTree<Int>, childTree: BinaryTree<Int>) ->Bool {
    //Assuming these are binary trees:
    //search tree1 from root until you find tree2.value
    //perhaps BFS on each node 1 level at a time, compare results and if it diverges, return false?
    
    if let childRoot = childTree.root {
        if let childInParent = parentTree.searchNodeWithValue(childRoot.value) {
            //At this point, we need to see if the trees are identical.  While we could do an in-order traversal of both and compare, that may take a ton of memory if the trees aren't matches.  Instead, we'll do BFS on both trees at the same time
            var parentQueue = Queue<TreeNode<Int>>()
            var childQueue = Queue<TreeNode<Int>>()
            parentQueue.enqueue(childInParent)
            childQueue.enqueue(childRoot)
            
            while !parentQueue.isEmpty() && !childQueue.isEmpty() {
                var parentNode = parentQueue.dequeue()!
                var childNode = childQueue.dequeue()!

                let parentLeft = parentNode.leftChild
                let parentRight = parentNode.rightChild
                let childLeft = childNode.leftChild
                let childRight = childNode.rightChild
                
                if parentLeft != nil || childLeft != nil {
                    if parentLeft?.value == childLeft?.value {
                        parentQueue.enqueue(parentLeft!)
                        childQueue.enqueue(childLeft!)
                    } else {
                        return false
                    }
                }

                if parentRight != nil || childRight != nil {
                    if parentRight?.value == childRight?.value {
                        parentQueue.enqueue(parentRight!)
                        childQueue.enqueue(childRight!)
                    } else {
                        return false
                    }
                }
            }
            return true
        } else {
            return false
        }
    } else {
        return false
    }
}


public func routeBetweenNodesInGraph(graph: [[Int]], startingNode: Int, endingNode: Int) -> Bool {
    var totalNodes = graph.count
    var q = Queue<Int>()
    if startingNode > totalNodes {
        return false
    }
    
    var visited = Array<Bool>(count:totalNodes, repeatedValue:false)

    q.enqueue(startingNode)
    visited[startingNode] = true
    while !q.isEmpty() {
        var currentNode = q.dequeue()!
        var neighbors = graph[currentNode]
        for  neighbor in neighbors {
            if neighbor == endingNode {
                return true
            } else {
                if !visited[neighbor] {
                    q.enqueue(neighbor)
                    visited[neighbor] = true
                }
            }
        }
        
    }
    
    
    return false
}