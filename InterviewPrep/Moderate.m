//
//  Moderate.m
//  InterviewPrep
//
//  Created by Robert Day on 3/14/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "Moderate.h"

@implementation Moderate


- (instancetype)init {
    self = [super init];
    if(self) {
        
    }
    return self;
}


//Design an algorithm to figure out if someone has won in a game of tic-tac-toe
//We'll consider a tic tac toe board to be a 3 x 3 array
//We only have to check 5 starting positions
//Check either down, right, or diagnol
- (BOOL)someoneWonTicTacToeWithBoard:(NSArray *)board {
    BOOL victor = NO;
    
    if([self someoneWonTicTacToeWithBoard:board fromRow:0 col:0]) {
        return YES;
    }
    if([self someoneWonTicTacToeWithBoard:board fromRow:0 col:1]) {
        return YES;
    }
    if([self someoneWonTicTacToeWithBoard:board fromRow:0 col:2]) {
        return YES;
    }
    if([self someoneWonTicTacToeWithBoard:board fromRow:1 col:1]) {
        return YES;
    }
    if([self someoneWonTicTacToeWithBoard:board fromRow:2 col:2]) {
        return YES;
    }
    return NO;
    
}
//0,0 | 0,1 | 0,2 | 1,0 | 1,1
- (BOOL)someoneWonTicTacToeWithBoard:(NSArray *)board fromRow:(NSInteger)row col:(NSInteger)col {
    NSInteger playerAtPosition = [board[row][col] integerValue];
    if(row == 0 && col == 0) { //Top left
        NSInteger midPlayer = [board[row + 1][col + 1] integerValue];
        if(midPlayer == playerAtPosition) {
            NSInteger bottomRightPlayer = [board[row + 2][col + 2] integerValue];
            if(bottomRightPlayer == playerAtPosition) {
                return YES;
            }
        }
    } else if (row == 0 && col == 2) { //top Right
        //Need to just check the diagnol; the other condition is checked below
        NSInteger midPlayer = [board[row + -1][col -1] integerValue];
        if(midPlayer == playerAtPosition) {
            NSInteger bottomLeftPlayer = [board[row - 2][col - 2] integerValue];
            if(bottomLeftPlayer == playerAtPosition) {
                return YES;
            }
        }
    }
    
    if(col == 0) { //Need to check right
        NSInteger playerRightOne = [board[row][col + 1] integerValue];
        if(playerAtPosition == playerRightOne) {
            NSInteger playerRightTwo = [board[row][col + 2] integerValue];
            if(playerAtPosition == playerRightTwo) {
                return YES;
            }
        }
        return NO;
    }
    if(row == 0){ //yPosition == 0 //Need to check  down
        NSInteger playerDownOne = [board[row + 1][col] integerValue];
        if(playerAtPosition == playerDownOne) {
            NSInteger playerDownTwo = [board[row + 2][col] integerValue];
            if(playerAtPosition == playerDownTwo) {
                return YES;
            }
        }
        return NO;
    }
    return NO;
}

//This implementation takes an array scan.  It's fine for this size of solution but if the length of the array was large, we'd want to add all possible solutions to a set
-(NSArray *)hitsAndPseudoHitsForSolution:(NSArray *)solution andGuess:(NSArray *)guess {
    NSInteger hits = 0;
    NSInteger pseudo = 0;
    for(NSInteger i = 0; i < guess.count; i++) {
        NSString *guessedCharacter = guess[i];
        NSString *correctCharacter = solution[i];
        if([guessedCharacter isEqualToString:correctCharacter]) {
            hits++;
        } else if([solution containsObject:guessedCharacter]) {
            pseudo++;
        }
    }
    return @[@(hits), @(pseudo)];
}
@end
