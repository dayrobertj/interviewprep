//
//  Chapter8.h
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Chapter8 : NSObject


+ (NSInteger)fibonacciNumberAtIndex:(NSInteger)index;
+ (NSArray *)allSubsetsOfSet:(NSArray *)set;

- (NSArray *)permutateString:(NSString *)str;
- (NSArray *)blah:(NSString *)str;
- (NSArray *)blah2:(NSString *)str index:(NSInteger)index;
- (NSArray *)allPermutationsForParens:(NSArray *)parens atIndex:(NSInteger)index;


- (NSArray *)paintFillWithArray:(NSArray *)grid startingPoint:(NSArray *)startingPoint color:(NSNumber *)color;
-(NSArray *)makeChangeForCents:(NSInteger)cents coinIndex:(NSInteger)index;
@end
