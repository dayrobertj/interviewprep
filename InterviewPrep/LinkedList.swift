//
//  LinkedList.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation
public class LinkedList {
    public var firstNode : ListNode?
    public func prependNode(node: ListNode) {
        if let myFirstNode = firstNode {
            node.next = myFirstNode
            firstNode = node
        } else {
            firstNode = node
        }
    }
    
    public func appendNode(node: ListNode) {
        if let myFirstNode = firstNode {
            var currentNode = myFirstNode
            while currentNode.next != nil {
                currentNode = currentNode.next
            }
            currentNode.next = node
        } else {
            firstNode = node
        }
    }
    
    public init() {
    
    }
}