//
//  UnsafeQueue.h
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnsafeQueue : NSObject

- (void)enqueueItem:(id)item;
- (id)dequeue;
- (BOOL)isEmpty;
@end
