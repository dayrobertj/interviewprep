//
//  MergeSort.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

//TODO: make it an array of things that are comparable

func mergeSort(inout input : [Int]) {
    mergeSort(&input, 0, input.count - 1)
}

func mergeSort(inout input : [Int], p: Int, r: Int) {
    if p < r {
        var mid = (p + r) / 2
        mergeSort(&input, p, mid)
        mergeSort(&input, mid+1, r)
        merge(&input, p, mid, r)
    }
}


private func merge(inout input: [Int], p: Int, q: Int, r: Int) {
    var left = [Int]()
    var right = [Int]()
    
    for var i = p; i <= q; i++ {
        left.append(input[i])
    }
    for var j = q+1; j <= r; j++ {
        right.append(input[j])
    }
    
    var k = p
    var i = 0
    var j = 0
    while i < left.count && j < right.count {
        if left[i] < right[j] {
            input[k] = left[i]
            i++
        } else {
            input[k] = right[j]
            j++
        }
        k++
    }
    
    
    while i < left.count {
        input[k] = left[i]
        i++
        k++
    }
    
    while j < right.count {
        input[k] = right[j]
        j++
        k++
    }
    
    
}