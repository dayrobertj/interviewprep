//
//  Chapter5.h
//  InterviewPrep
//
//  Created by Robert Day on 3/8/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Chapter5 : NSObject


- (NSInteger)setN:(NSInteger)n toM:(NSInteger)m fromI:(NSInteger)i toJ:(NSInteger)j;
@end
