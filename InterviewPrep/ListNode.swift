//
//  Node.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/1/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class ListNode {
    public var value : AnyObject!
    public var next : ListNode!
    
    public init(value: AnyObject) {
        self.value = value
    }
}