//
//  RandomProblems.m
//  InterviewPrep
//
//  Created by Robert Day on 3/17/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "RandomProblems.h"

@interface RandomProblems ()

@property(nonatomic, strong) NSArray *characterMap;

@end

@implementation RandomProblems

- (instancetype)init {
    self = [super init];
    if(self) {
        _characterMap = @[@[@""],
                          @[@""],
                          @[@"A",@"B",@"C"],
                          @[@"D",@"E",@"F"],
                          @[@"G",@"H",@"I"],
                          @[@"J",@"K",@"L"],
                          @[@"M",@"N",@"O"],
                          @[@"P",@"Q",@"R", @"S"],
                          @[@"T",@"U",@"V"],
                          @[@"W",@"X",@"Y", @"Z"]];
    }
    return self;
}
//Given an SMS text (array of digits) what are the possible words that could be created?

-(NSArray *)wordPermutationsForNumber:(NSArray *)number {
    return [self wordPermutationsForNumber:number startingIndex:0];
}

-(NSArray *)wordPermutationsForNumber:(NSArray *)number startingIndex:(NSInteger)startingIndex {
    //Base case
    if(startingIndex == number.count) {
        return @[@""];
    }
    
    NSMutableArray *resultingWords = [NSMutableArray new];
    NSInteger digitInt = [number[startingIndex] integerValue];
    NSArray *permutations = [self wordPermutationsForNumber:number startingIndex:startingIndex+1];
    
    for(NSString *character in self.characterMap[digitInt]) {
        for(NSString *permutation in permutations) {
            NSString *newString = [NSString stringWithFormat:@"%@%@", character, permutation];
            [resultingWords addObject:newString];
        }
    }
    
    return resultingWords;
}

//1) Given an array with 0s and other integers. Find the fastest (that use least organizing) way to transfer all the 0s to the right end of that array.

- (void)move0sInArrayToRight:(NSMutableArray *)array {
    
    NSInteger zeroPointer = array.count - 1;
    for(NSInteger i = array.count-1; i >= 0; i--) {
        if([array[i] integerValue] == 0) {
            NSInteger placeholder = [array[i] integerValue];
            array[i] = array[zeroPointer];
            array[zeroPointer] = @(placeholder);
            zeroPointer--;
        }
    }
}

//return the sum of the k largest digits in an array
-(NSInteger)sumUpLastK:(NSInteger)k integersOfArray:(NSArray *)array {
    return [self sumUpFromIndexK:array.count - k integersOfArray:[array mutableCopy] startingIndex:0 endingIndex:array.count - 1];
}

-(NSInteger)sumUpFromIndexK:(NSInteger)k integersOfArray:(NSMutableArray *)array startingIndex:(NSInteger)startingIndex endingIndex:(NSInteger)endingIndex {
    NSInteger targetIndex = k;
    NSInteger pivot = [self partitionArray:array fromStartingIndex:startingIndex endingIndex:endingIndex];
    if(pivot == targetIndex) {
        //The array from pivot to the end needs to be summed
        NSInteger sum = 0;
        for(NSInteger i = pivot; i < array.count; i++) {
            sum += [array[i] integerValue];
        }
        return sum;
    } else if(pivot < k) {
        return [self sumUpFromIndexK:k integersOfArray:array startingIndex:pivot + 1 endingIndex:endingIndex];
    } else {
        return [self sumUpFromIndexK:k integersOfArray:array startingIndex:startingIndex endingIndex:pivot - 1];
    }
}

-(NSInteger)partitionArray:(NSMutableArray *)array fromStartingIndex:(NSInteger)startingIndex endingIndex:(NSInteger)endingIndex {
    NSInteger pivotIndex = endingIndex;
    NSInteger pivotValue = [array[pivotIndex] integerValue];
    
    NSInteger lowBorder = startingIndex;
    for(NSInteger i = startingIndex; i <= endingIndex; i++) {
        if([array[i] integerValue] < pivotValue) {
            NSInteger placeholder = [array[i] integerValue];
            array[i] = array[lowBorder];
            array[lowBorder] = @(placeholder);
            lowBorder++;
        }
    }
    NSInteger placeholder = [array[lowBorder] integerValue];
    array[lowBorder] = array[pivotIndex];
    array[pivotIndex] = @(placeholder);
    
    return lowBorder;
    
}
@end
