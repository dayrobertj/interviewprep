//
//  RandomProblems.h
//  InterviewPrep
//
//  Created by Robert Day on 3/17/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomProblems : NSObject

-(NSArray *)wordPermutationsForNumber:(NSArray *)number;
- (void)move0sInArrayToRight:(NSMutableArray *)array;
-(NSInteger)sumUpLastK:(NSInteger)k integersOfArray:(NSArray *)array;
@end
