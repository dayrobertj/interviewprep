//
//  InterviewPrepLib.h
//  InterviewPrepLib
//
//  Created by Robert Day on 3/4/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for InterviewPrepLib.
FOUNDATION_EXPORT double InterviewPrepLibVersionNumber;

//! Project version string for InterviewPrepLib.
FOUNDATION_EXPORT const unsigned char InterviewPrepLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <InterviewPrepLib/PublicHeader.h>


