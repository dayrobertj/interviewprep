//
//  NSInteger+Binary.h
//  InterviewPrep
//
//  Created by Robert Day on 3/9/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber(Binary)
- (NSString *)binaryRespresentation;
@end
