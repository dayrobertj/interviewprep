//
//  RandomSwiftProblems.swift
//  InterviewPrep
//
//  Created by Robert Day on 3/17/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

import Foundation

public class RandomSwiftProblems {
    
    public init() {}
    
    //Print a linked list in reverse recursively and non-destructively
    
    public func printLinkedListReversedRecursively(list: LinkedList, currentNode: ListNode?) {
        if let currentNode = currentNode {
            self.printLinkedListReversedRecursively(list, currentNode: currentNode.next)
            println("\(currentNode.value)")
        }
    }
    
    
    //Reverse a linked list
    public func reverseLinkedList(list: LinkedList) -> LinkedList {
        var newList = LinkedList()
        var currentNode = list.firstNode
        var nextPlaceholder : ListNode
        while currentNode != nil {
            newList.prependNode(currentNode!)
            currentNode = currentNode!.next
        }
        return newList
    }
    
    
}