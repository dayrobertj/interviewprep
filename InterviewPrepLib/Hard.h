//
//  Hard.h
//  InterviewPrep
//
//  Created by Robert Day on 3/14/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hard : NSObject

- (NSArray *)shuffledDeck;
- (NSInteger)shortestDistanceBetweenWordsInBook:(NSArray *)book withFirstWord:(NSString *)firstWord secondWord:(NSString *)secondWord;
- (NSString *)longestWordFromOtherWordsInArray:(NSArray *)words;

- (NSArray *)largestBlackSubsquareInArray:(NSArray *)array;
@end
