//
//  Hard.m
//  InterviewPrep
//
//  Created by Robert Day on 3/14/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "Hard.h"


@interface MatrixPoint : NSObject <NSCopying>
@property(nonatomic) NSInteger row;
@property(nonatomic) NSInteger col;
- (instancetype)initWithRow:(NSInteger)row col:(NSInteger)col;
@end

@implementation MatrixPoint

- (instancetype)initWithRow:(NSInteger)row col:(NSInteger)col {
    self = [super init];
    if(self) {
        _row = row;
        _col = col;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

@end

@implementation Hard



-(instancetype)init {
    self = [super init]; return self;
}



//Write a method to shuffle a deck of cards It must be a perfect shuffle - in other words, each 52! permutations of the deck ha sto be equally likely Assume that you are given a random number generator which is perfect

- (NSArray *)shuffledDeck {
    NSMutableOrderedSet *unshuffledDeck = [[NSMutableOrderedSet alloc] initWithArray:[self unshuffledDeck]];
    NSMutableArray *shuffledDeck = [NSMutableArray new];
    for(int i = 0; i < 52; i++) {
        NSInteger cardIndex = arc4random_uniform(52 - i);
        [shuffledDeck addObject:unshuffledDeck[cardIndex]];
        [unshuffledDeck removeObjectAtIndex:cardIndex];
    }
    
    return [shuffledDeck copy];
}

- (NSArray *)unshuffledDeck {
    NSMutableArray *deck = [NSMutableArray new];
    for(NSInteger i = 0; i < 52; i++) {
        [deck addObject:@(i)];
    }
    return [deck copy];
}



//Write a method to randomly generate a set of m integers from an array of size n Each element must have equal probability of being chosen
//Pick a random index between 0 and n-1.  Take that number and add it to set m
// swap picked index & last index
// Pick a random number between 0 and n-1-(set.count)
// Repeated until M is correct size
// My solutoin works


//You have a large text file containing words Given any two words, find the shortest distance (in terms of number of words) between them in the file Can you make the searching operation in O(1) time? What about the space complexity for your solu- tion?

//Trivial way with no extra storage is to search for all occurences of either word.  Then, do the same subroutine that will be found here
//This is case-sensitive
- (NSInteger)shortestDistanceBetweenWordsInBook:(NSArray *)book withFirstWord:(NSString *)firstWord secondWord:(NSString *)secondWord {
    NSMutableDictionary *positions = [[NSMutableDictionary alloc] init];
    //Let's hydrate our positoins for each word
    for(int i = 0; i < book.count; i++) {
        NSString *word = book[i];
        NSMutableArray *positionsForWord = positions[word];
        if(!positionsForWord ) {
            positionsForWord = [NSMutableArray new];
            positions[word] = positionsForWord;
        }
        [positionsForWord addObject:@(i)];
        [positionsForWord sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return obj1 > obj2;
        }];
    }
    
    //Assume that cache was populated during init or something
    
    NSArray *positionsForFirstWord = positions[firstWord];
    NSArray *positionsForSecondWord = positions[secondWord];
    if(positionsForFirstWord.count == 0 || positionsForSecondWord == 0) {
        return NSIntegerMax;
    }
    //comparing: 2 4  6  8
    //           5 10 11 12
    NSInteger minDistance = NSIntegerMax;
    for(NSInteger i = 0; i < positionsForFirstWord.count; i++) {
        NSNumber *firstValue = positionsForFirstWord[i];
        NSInteger previousDistance = NSIntegerMax;
        for(NSInteger j = 0; j < positionsForSecondWord.count; j++) {
            NSNumber *secondValue = positionsForSecondWord[j];
            NSInteger difference = abs(firstValue.intValue - secondValue.intValue);
            if(difference < minDistance) {
                minDistance = difference;
            } else if (previousDistance < difference) {
                //If the previous distance was less than the current, break as we're dealing with sorted arrays and our value can't get better
                break;
            }
            previousDistance = difference;
        }
    }
    return minDistance;

}


//Write a program to find the longest word made of other words in a list of words
//EXAMPLE
//Input: test, tester, testertest, testing, testingtester
//Output: testingtester

//BLAH: possible optimization I didn;t see: Instead  of iterating over the full array again, we can take all permutations of each string and check set for both

- (NSString *)longestWordFromOtherWordsInArray:(NSArray *)words {
    NSMutableSet *allWords = [NSMutableSet new];
    for(NSString *word in words) {
        [allWords addObject:word];
    }
    NSArray *sortedWords = [words sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSString *firstWord = (NSString *)obj1;
        NSString *secondWord = (NSString *)obj2;
        return firstWord.length > secondWord.length;
    }];
    
    
    for(NSInteger i = sortedWords.count - 1; i >=0; i--) {
        NSString *baseWord = sortedWords[i];
        for(NSInteger j = i-1; j >=0; j--) {
            NSString *subWord = sortedWords[j];
            NSRange range = [baseWord  rangeOfString:subWord];
            if(range.location != NSNotFound) {
                NSString *remainderBase = [baseWord substringToIndex:range.location];
                NSString *remainderEnd = [baseWord substringFromIndex:range.location + range.length];
                NSString *remainderString = [NSString stringWithFormat:@"%@%@", remainderBase, remainderEnd];
                if([allWords containsObject:remainderString]) {
                    return baseWord;
                }
            }
        }
    }
    return nil;
}

//B B B W W
//W B B W W
//W B B B W
//W B W B W
//B B B W B
//
//W B B W W
//W B B B W
//W B B B W
//W B B B W
//B B B W B
- (NSArray *)largestBlackSubsquareInArray:(NSArray *)array {
    NSInteger dimension = array.count;
    NSInteger maxSquare = 0;
    for(NSInteger row = 0; row < dimension - 1; row++) {
        for(NSInteger col = 0; col < dimension -1; col++) {
            NSInteger squareSize = [self largestBlackSubarrayForArray:array fromRow:row col:col];
            if(squareSize > maxSquare) {
                maxSquare = squareSize;
            }

        }
    }

    
    return @[];
}

//W B B W W
//W B B B W
//W B B B W
//W B B B W
//B B B W B
- (NSInteger)largestBlackSubarrayForArray:(NSArray *)array fromRow:(NSInteger)row col:(NSInteger)col {

    if(![array[row][col] isEqualToString:@"B"]) return 0;
    NSInteger blackWidth = 1;
    while(true) {
        if(col + blackWidth == array.count) break;
        if([array[row][col + blackWidth] isEqualToString:@"B"]) {
            blackWidth ++;
        } else {
            break;
        }
    }
    
    NSInteger blackHeight = 1;
    //Now we know the maximum black square going from left to right
    BOOL hitW = NO; //Track whether or not we've ever failed to hit a "B"
        //If hitW is true, no point to proceed with any more tests
    for(NSInteger newRow = row + 1; row < array.count; newRow++) {
        if(hitW) break;
        if(newRow - row > blackWidth || blackWidth == blackHeight) {
            //End early if:
                //We've too big of a square by rows
                //We've already met the max width to max heights
            break;
        }
        for(NSInteger newCol = col; col < array.count; newCol++) {
            if([array[newRow][newCol] isEqualToString:@"B"]) {
                if(newCol - col + 1 > blackHeight) {
                    //if the new col distance is greater than the previous best
                    blackHeight = newCol - col + 1;
                }
                if(blackWidth == blackHeight) {
                    break;
                }
            } else {
                hitW = YES;
                break;
            }

        }
    }
    
    return blackHeight;
}


@end
