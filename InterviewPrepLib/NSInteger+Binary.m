//
//  NSInteger+Binary.m
//  InterviewPrep
//
//  Created by Robert Day on 3/9/15.
//  Copyright (c) 2015 Robert Day. All rights reserved.
//

#import "NSInteger+Binary.h"

@implementation NSNumber (Binary)

- (NSString *)binaryRespresentation {
    NSInteger value = self.integerValue;
    NSMutableString *str = [NSMutableString string];
    for(NSInteger numberCopy = value; numberCopy > 0; numberCopy >>= 1)
    {
        // Prepend "0" or "1", depending on the bit
        [str insertString:((numberCopy & 1) ? @"1" : @"0") atIndex:0];
    }
    
    return str;
}
@end
